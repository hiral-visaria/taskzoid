@extends('layouts.template')

@section('title', 'Admin | Manage Team Members')

@section('content')
    @include('layouts.partials._message')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Members</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="manage_members" width="100%" cellspacing="0">
                    <div id="export-buttons"></div>
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Performance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('styles')
<style>
    #export-buttons .dt-buttons.btn-group.flex-wrap {
        position: absolute;
        left: 84.4%;
        top: .4rem;
    }

    #export-buttons .btn.buttons-html5 {
        background-color: #FFF;
        border-color: #CCC;
        color: #333;
    }
</style>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var TableDatatables = function(){
            var handleMembersTable = function(){
                var oTable = $('#manage_members').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('cpanel.datatable.members', $team->id) }}",
                    columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                        },
                        {
                            data: 'image',
                            name: 'image',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'performance',
                            name: 'performance'
                        },
                        {
                            data: 'actions',
                            name: 'actions',
                            orderable: false,
                            searchable: false
                        }
                    ]
                });
                new $.fn.dataTable.Buttons(oTable, {
                    buttons:[
                        'copy', 'csv', 'pdf'
                    ]
                } );
                oTable.buttons().container()
                    .appendTo( $('#export-buttons') );
            }
            return {
                //main function to handle all the datatables
                init: function () {
                    handleMembersTable();
                }
            }
        }();
        jQuery(document).ready(function () {
            TableDatatables.init();
        });
    });
</script>
@endsection
