<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Projects\CreateProjectRequest;
use App\Notifications\ProjectCreationNotification;
use App\Project;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function create(Team $team)
    {
        $this->authorize('create', Project::class);

        return view('admin.projects.create', compact([
            'team'
        ]));
    }

    public function store(CreateProjectRequest $request, Team $team)
    {
        // dd($team->id);
        $project = Project::create([
            'team_id' => $team->id,
            'name' => $request->name,
            'description' => $request->description,
            'tags' => $request->tags,
            'due' => $request->due
        ]);

        $member = User::where('team_id', $team->id)->where('role', 'leader')->first();
        if($member){
            $member->notify(new ProjectCreationNotification($project));
        }

        $projects = Project::where('team_id', $team->id)->latest()->get();

        session()->flash('success', "Project Created Successfully");
        return redirect(route('cpanel.projects.index', compact([
            'team',
            'projects'
        ])));
    }
}
