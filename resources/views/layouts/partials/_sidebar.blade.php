<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center"
        href="
            @if(auth()->user()->isAdmin())
                {{ route('cpanel.dashboard') }}
            @elseif(auth()->user()->isLeader())
                {{ route('leader.dashboard') }}
            @else
                {{ route('member.dashboard') }}
            @endif
        ">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Ticket Management</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    @if (auth()->user()->isAdmin())
        <li class="nav-item">
        <a class="nav-link" href="{{ route('cpanel.dashboard') }}">
            <i class="fa fa-home"></i>
            <span>Dashboard</span></a>
        </li>
    @endif

    @if (auth()->user()->isLeader())
        <li class="nav-item">
        <a class="nav-link" href="{{ route('leader.dashboard') }}">
            <i class="fa fa-home"></i>
            <span>Dashboard</span></a>
        </li>
    @endif

    @if (auth()->user()->isMember() || auth()->user()->isNaive())
        <li class="nav-item">
        <a class="nav-link" href="{{ route('member.dashboard') }}">
            <i class="fa fa-home"></i>
            <span>Dashboard</span></a>
        </li>
    @endif

    <!-- Divider -->
    <hr class="sidebar-divider">

    @if (auth()->user()->isAdmin())
        <li class="nav-item">
        <a class="nav-link" href="{{ route('cpanel.teams.index') }}">
            <i class="fa fa-users"></i>
            <span>Teams</span></a>
        </li>
    @endif

    @if (auth()->user()->isAdmin())
        <li class="nav-item">
        <a class="nav-link" href="{{ route('users.index') }}">
            <i class="fa fa-user"></i>
            <span>Users</span></a>
        </li>
    @endif

    @if (auth()->user()->isLeader())
        <li class="nav-item">
        <a class="nav-link" href="{{ route('leader.projects.index') }}">
            <i class="fas fa-chart-line"></i>
            <span>Projects</span></a>
        </li>
    @endif

    @if (auth()->user()->isMember() && auth()->user()->tickets->count())
        <li class="nav-item">
        <a class="nav-link" href="{{ route('member.projects.index') }}">
            <i class="fas fa-chart-line"></i>
            <span>Projects</span></a>
        </li>
    @endif

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>


