@extends('layouts.template')

@section('title', 'Admin | Dashboard')

@section('content')
    @include('layouts.partials._message')
    <div class="row">
        <div class="col-md-12 mb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col mr-2 d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="mr-3">
                                    @if($user->image == null)
                                        <img src="{{ asset("storage/users/neutral.png") }}" class="rounded" width="128px" alt="image">
                                    @else
                                        <img src="{{ asset("storage/" . $user->image) }}" class="rounded" width="128px" alt="image">
                                    @endif
                                </div>
                                <div class="text-gray-600">
                                    <p class="mb-0">
                                        {{ $user->name }}
                                        <i class="fas fa-check-circle" style="color: green"></i>
                                    </p>
                                    <p>
                                        <span><i class="far fa-envelope"></i> {{ $user->email }}</span>
                                    </p>
                                </div>
                            </div>
                            <div>
                                <a href="{{ route('cpanel.edit', $user->id) }}" class="btn btn-outline-success btn-sm btn-upper">Edit</a>&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 mb-4">
            <div class="card shadow h-100">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Best Member</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col mr-2 d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="mr-3">
                                    @if($bestMember[0]->image == null)
                                        <img src="{{ asset("storage/users/neutral.png") }}" class="rounded" width="128px" alt="image">
                                    @else
                                        <img src="{{ asset("storage/" . $bestMember[0]->image) }}" class="rounded" width="128px" alt="image">
                                    @endif
                                </div>
                                <div class="text-gray-600">
                                    <p class="mb-0">
                                        {{ $bestMember[0]->name }}
                                    </p>
                                    <p class="mb-0">
                                        <span><i class="far fa-envelope"></i> {{ $bestMember[0]->email }}</span>
                                    </p>
                                    <p class="font-weight-bold text-success mb-0">
                                        <span>Performance: {{ round($bestMember[0]->ticketCalculation, 2) }}%</span>
                                    </p>
                                </div>
                            </div>
                            <div class="text-right">
                                <p class="mb-0 mr-2 text-primary font-weight-bold">Team: {{ $bestMemberTeam->name }}</p>
                                <p class="mb-0 mr-2 text-warning font-weight-bold">Projects: {{ $bestMemberProjects->count() }}</p>
                                <p class="mb-0 mr-2 text-info font-weight-bold">Tickets: {{ $bestMemberTickets->count() }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--Begin::Section-->
    {{-- <div class="row pl-3 pr-3">
        <div class="kt-portlet">
            <div class="kt-portlet__body  kt-portlet__body--fit">
                <div class="row row-no-padding row-col-separator-xl">
                    <div class="col-xl-4">

                        <!--begin:: Widgets/Daily Sales-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header kt-margin-b-30">
                                    <h3 class="kt-widget14__title">
                                        Daily Sales
                                    </h3>
                                    <span class="kt-widget14__desc">
                                        Check out each collumn for more details
                                    </span>
                                </div>
                                <div class="kt-widget14__chart" style="height:120px;">
                                    <canvas id="kt_chart_daily_sales"></canvas>
                                </div>
                            </div>
                        </div>

                        <!--end:: Widgets/Daily Sales-->
                    </div>
                    <div class="col-xl-4">

                        <!--begin:: Widgets/Profit Share-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                        Profit Share
                                    </h3>
                                    <span class="kt-widget14__desc">
                                        Profit Share between customers
                                    </span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                        <div class="kt-widget14__stat">45</div>
                                        <canvas id="kt_chart_profit_share" style="height: 140px; width: 140px;"></canvas>
                                    </div>
                                    <div class="kt-widget14__legends">
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-success"></span>
                                            <span class="kt-widget14__stats">37% Sport Tickets</span>
                                        </div>
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-warning"></span>
                                            <span class="kt-widget14__stats">47% Business Events</span>
                                        </div>
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-brand"></span>
                                            <span class="kt-widget14__stats">19% Others</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end:: Widgets/Profit Share-->
                    </div>
                    <div class="col-xl-4">

                        <!--begin:: Widgets/Revenue Change-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                        Revenue Change
                                    </h3>
                                    <span class="kt-widget14__desc">
                                        Revenue change breakdown by cities
                                    </span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                        <div id="kt_chart_revenue_change" style="height: 150px; width: 150px;"></div>
                                    </div>
                                    <div class="kt-widget14__legends">
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-success"></span>
                                            <span class="kt-widget14__stats">+10% New York</span>
                                        </div>
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-warning"></span>
                                            <span class="kt-widget14__stats">-7% London</span>
                                        </div>
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-brand"></span>
                                            <span class="kt-widget14__stats">+20% California</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end:: Widgets/Revenue Change-->
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <!--End::Section-->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Teams</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="manage_teams" width="100%" cellspacing="0">
                    <div id="export-buttons"></div>
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Team Name</th>
                            <th>Team Leader</th>
                            <th>Projects</th>
                            <th>Tickets</th>
                            <th>Performance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Logs</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="manage_logs" width="100%" cellspacing="0">
                    <div id="export-buttons"></div>
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Logged At</th>
                            <th>Login Time</th>
                            <th>Logout Time</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style>
        #export-buttons .dt-buttons.btn-group.flex-wrap {
            position: absolute;
            left: 84.4%;
            top: .4rem;
        }

        #export-buttons .btn.buttons-html5 {
            background-color: #FFF;
            border-color: #CCC;
            color: #333;
        }
    </style>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var TableDatatables = function(){
            var handleLogsTable = function(){
                var oTable = $('#manage_logs').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('cpanel.datatable.logs') }}",
                    columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                        },
                        {
                            data: 'logged_at',
                            name: 'logged_at'
                        },
                        {
                            data: 'login_time',
                            name: 'login_time'
                        },
                        {
                            data: 'logout_time',
                            name: 'logout_time'
                        }
                    ]
                });
                new $.fn.dataTable.Buttons(oTable, {
                    buttons:[
                        'copy', 'csv', 'pdf'
                    ]
                } );
                oTable.buttons().container()
                    .appendTo( $('#export-buttons') );
            }

            var handleTeamsTable = function(){
                var oTable = $('#manage_teams').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('cpanel.datatable.teams') }}",
                    columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'team_leader',
                            name: 'team_leader'
                        },
                        {
                            data: 'projects',
                            name: 'projects'
                        },
                        {
                            data: 'tickets',
                            name: 'tickets'
                        },
                        {
                            data: 'performance',
                            name: 'performance'
                        },
                        {
                            data: 'actions',
                            name: 'actions',
                            orderable: false,
                            searchable: false
                        }
                    ]
                });
                new $.fn.dataTable.Buttons(oTable, {
                    buttons:[
                        'copy', 'csv', 'pdf'
                    ]
                } );
                oTable.buttons().container()
                    .appendTo( $('#export-buttons') );
            }

            return {
                //main function to handle all the datatables
                init: function () {
                    handleLogsTable();
                    handleTeamsTable();
                }
            }
        }();
        jQuery(document).ready(function () {
            TableDatatables.init();
        });
    });
</script>
@endsection
