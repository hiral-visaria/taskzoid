<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'project_id',
        'member_id',
        'title',
        'description',
        'labels',
        'due',
        'difficulty_level',
        'status',
        'assigned_at',
        'resolved_at',
        'attachments',
    ];

    protected $dates = [
        'due',
        'assigned_at',
        'resolved_at'
    ];

    public function getDueDateAttribute() {
        return $this->due->diffForHumans();
    }

    public function getAssignedDateAttribute() {
        return $this->assigned_at->diffForHumans();
    }

    public function getResolvedDateAttribute() {
        return $this->resolved_at->diffForHumans();
    }

    public function project() {
        return $this->belongsTo(Project::class);
    }

    public function member() {
        return $this->belongsTo(User::class, 'member_id');
    }
}
