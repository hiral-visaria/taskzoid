@extends('layouts.template')

@section('title', 'Leader | Edit Ticket')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <!-- CARD HEADER -->
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-plus"></i> Add Ticket
                </h6>
            </div>
            <!-- END OF CARD HEADER -->

            <!-- CARD BODY -->
            <div class="card-body">
                <form class="kt-form" action="{{ route('tickets.update', [$project->id, $ticket->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" placeholder="Enter ticket title" value="{{old('title', $ticket->title)}}"
                                   class="form-control @error('title') is-invalid @enderror"
                                   name="title" id="title">
                            @error('title')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" id="description" name="description" placeholder="Enter Description">{{ old('description', $ticket->description) }}</textarea>
                            @error('description')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Labels</label>
                            <input type="text" placeholder="Eg. database, testing" value="{{old('labels', $ticket->labels)}}"
                                   class="form-control @error('labels') is-invalid @enderror"
                                   name="labels" id="labels">
                            @error('labels')
                                <p class="text-danger">{{ $message }}</p>
                            @else
                                <span class="text-danger">More than one label should be entered as comma separated values</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="due">Due</label>
                            <input type="text" value="{{old('due', $ticket->due)}}" class="form-control" name="due" id="due">
                            @error('due')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="difficulty_level">Difficulty Level</label>
                            <select name="difficulty_level" id="difficulty_level" class="form-control select2">
                                <option value="" disabled selected>Select Difficulty Level</option>
                                <option value="high"  @if(old('difficulty_level', $ticket->difficulty_level) == "high") selected @endif>High</option>
                                <option value="moderate"  @if(old('difficulty_level', $ticket->difficulty_level) == "moderate") selected @endif>Moderate</option>
                                <option value="low" @if(old('difficulty_level', $ticket->difficulty_level) == "low") selected @endif>Low</option>
                            </select>
                            @error('difficulty_level')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="status form-control select2" onchange="getMembers({{ auth()->user()->team_id }})">
                                <option disabled selected>Select Status</option>
                                <option value="unassigned" @if(old('status', $ticket->status) == "unassigned") selected @endif>Unassigned</option>
                                <option value="assigned"  @if(old('status', $ticket->status) == "assigned") selected @endif>Assigned</option>
                            </select>
                            @error('status')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group" id="members"></div>

                        <div class="form-group d-none" id="assignment">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" id="automatic_assignment" onchange="automaticAssignment({{ auth()->user()->team_id }})"> Automatic Assignment
                                <span></span>
                            </label>
                        </div>

                        <div class="form-group text-muted" id="assigned_to"></div>
                    </div>

                    <button type="submit" class="btn btn-outline-primary btn-sm m-1"><i class='fa fa-pen'></i> Update</button>
                </form>
            </div>
            <!-- END OF CARD BODY -->
        </div>
    </div>
</div>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr("#due", {
            enableTime: true
        });

        if(window.location.reload) {
            getMembers({{ auth()->user()->team_id }});
        }

        function getMembers(teamID)
        {
            status = $('.status option:selected').val();
            console.log(teamID);
            if(status == "unassigned") {
                console.log(status);
                $('#members').empty();
                $('#assignment').remove();
            }else{
                console.log(status);
                route = "{{ route('leader.ajax.getdata') }}";
                memberID = "{{ $ticket->member_id }}";
                oldMemberID = "{{ old('member_id') }}";
                $.ajax({
                    url: route,
                    method: "POST",
                    data:{
                        _token: "{{ csrf_token() }}",
                        getMembers: true,
                        teamID: teamID
                    },
                    dataType: 'json',
                    success: function(members){
                        console.log(members);

                        $('#members').empty();

                        teamMembers = `
                            <label for="member_id">Member</label>
                            <select name="member_id" id="member_id" class="form-control">
                                <option disabled selected>Select Member</option>
                        `;

                        for(i = 0; i < members.length; i++) {
                            if(oldMemberID == members[i]['id'])
                                teamMembers += `<option value='${members[i]['id']}' selected>${members[i]['name']}</option>`;
                            else if(memberID == members[i]['id'])
                                teamMembers += `<option value='${members[i]['id']}' selected>${members[i]['name']}</option>`;
                            else
                                teamMembers += `<option value='${members[i]['id']}'>${members[i]['name']}</option>`;
                        }
                        teamMembers += `
                            </select>
                            @error('member_id')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>`;

                        $("#members").append(teamMembers);
                        $('#member_id').select2();
                        $("#assignment").removeClass("d-none");
                    }
                });
            }
        }

        function automaticAssignment(teamID) {
            if($("#automatic_assignment").is(':checked')) {
                $("#member_id").attr('disabled', true);

                route = "{{ route('leader.ajax.getdata') }}";
                $.ajax({
                    url: route,
                    method: "POST",
                    data:{
                        _token: "{{ csrf_token() }}",
                        getBestOrAvailableMember: true,
                        teamID: teamID
                    },
                    dataType: 'json',
                    success: function(member){
                        console.log(member);

                        eligibleMember = `
                            <input type="hidden" name="member_id" value=${member.id}>
                            <span>Assigning To : ${member.name}</span>
                        `;

                        $("#assigned_to").append(eligibleMember);
                    }
                });
            } else {
                $("#assigned_to").empty();
                $("#member_id").attr('disabled', false);
            }
        }

        $('.select2').select2();
    </script>
@endsection
