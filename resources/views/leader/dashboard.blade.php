@extends('layouts.template')

@section('title', 'Leader | Dashboard')

@section('content')
    @include('layouts.partials._message')
    <div class="row">
        <div class="col-md-12 mb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col mr-2 d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="mr-3">
                                    @if($user->image == null)
                                        <img src="{{ asset("storage/users/neutral.png") }}" class="rounded" width="128px" alt="image">
                                    @else
                                        <img src="{{ asset("storage/" . $user->image) }}" class="rounded" width="128px" alt="image">
                                    @endif
                                </div>
                                <div class="text-gray-600">
                                    <p class="mb-0">
                                        {{ $user->name }}
                                    </p>
                                    <p>
                                        <span><i class="far fa-envelope"></i> {{ $user->email }}</span>
                                    </p>
                                    <p class="btn btn-outline-success rounded btn-sm btn-upper">Projects: <span>{{ $projects->count() }}</span></p>&nbsp;
                                </div>
                            </div>
                            <div class="">
                                <a href="{{ route('leader.edit', $user->id) }}" class="btn btn-outline-success btn-sm btn-upper">Edit</a>&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 mb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body pt-2 pb-2">
                    <div class="row" id="justify-content-evenly">
                        <div class="col-md-2 d-flex">
                            <div class="mr-3 mt-4">
                                <i class="far fa-file-code text-xl"></i>
                            </div>
                            <div class="mt-3">
                                <p class="mb-0">Approved Tickets</p>
                                <a href="javascript:void(0)" onclick="getTickets('approved')" class="text-info font-weight-bold">View</a>
                            </div>
                        </div>

                        <div class="col-md-2 d-flex">
                            <div class="mr-3 mt-4">
                                <i class="far fa-file-code text-xl"></i>
                            </div>
                            <div class="mt-3">
                                <p class="mb-0">Resolved Tickets</p>
                                <a href="javascript:void(0)" onclick="getTickets('resolved')" class="text-info font-weight-bold">View</a>
                            </div>
                        </div>

                        <div class="col-md-2 d-flex">
                            <div class="mr-3 mt-4">
                                <i class="far fa-file-code text-xl"></i>
                            </div>
                            <div class="mt-3">
                                <p class="mb-0">Unresolved Tickets</p>
                                <a href="javascript:void(0)" onclick="getTickets('unresolved')" class="text-info font-weight-bold">View</a>
                            </div>
                        </div>

                        <div class="col-md-2 d-flex">
                            <div class="mr-3 mt-4">
                                <i class="far fa-file-code text-xl"></i>
                            </div>
                            <div class="mt-3">
                                <p class="mb-0">Assigned Tickets</p>
                                <a href="javascript:void(0)" onclick="getTickets('assigned')" class="text-info font-weight-bold">View</a>
                            </div>
                        </div>

                        <div class="col-md-2 d-flex">
                            <div class="mr-3 mt-4">
                                <i class="fas fa-chart-area text-xl"></i>
                            </div>
                            <div class="mt-3">
                                <p class="mb-0">Performance</p>
                                <span class="font-weight-bold text-info">
                                    {{ (sizeof($performance) ? (round($performance[0]->ticketCalculation, 2)) : 0) }} %
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Begin::Section-->
    <div class="row">
        <div id="tickets" class="col-md-12">
        </div>
    </div>
    <!--End::Section-->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Members</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="manage_members" width="100%" cellspacing="0">
                    <div id="export-buttons"></div>
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Performance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Logs</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="manage_logs" width="100%" cellspacing="0">
                    <div id="export-buttons-logs"></div>
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Logged At</th>
                            <th>Login Time</th>
                            <th>Logout Time</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


@section('styles')
    <style>
        #export-buttons .dt-buttons.btn-group.flex-wrap,
        #export-buttons-logs .dt-buttons.btn-group.flex-wrap {
            position: absolute;
            left: 84.4%;
            top: .4rem;
        }

        #export-buttons .btn.buttons-html5,
        #export-buttons-logs .btn.buttons-html5 {
            background-color: #FFF;
            border-color: #CCC;
            color: #333;
        }

        #justify-content-evenly {
            display: flex;
            justify-content: space-evenly
        }

        .text-xl {
            font-size: 2rem!important;
        }

        a:hover,
        a:active,
        a:focus {
            text-decoration: none;
        }
    </style>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var TableDatatables = function(){
            var handleLogsTable = function(){
                var testTable = $('#manage_logs').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('leader.datatable.logs') }}",
                    columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                        },
                        {
                            data: 'logged_at',
                            name: 'logged_at'
                        },
                        {
                            data: 'login_time',
                            name: 'login_time'
                        },
                        {
                            data: 'logout_time',
                            name: 'logout_time'
                        }
                    ]
                });
                new $.fn.dataTable.Buttons(testTable, {
                    buttons:[
                        'copy', 'csv', 'pdf'
                    ]
                } );
                testTable.buttons().container()
                    .appendTo( $('#export-buttons-logs') );
            }

            var handleTeamsTable = function(){
                var oTable = $('#manage_members').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('leader.datatable.members') }}",
                    columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                        },
                        {
                            data: 'image',
                            name: 'image',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'performance',
                            name: 'performance'
                        },
                        {
                            data: 'actions',
                            name: 'actions',
                            orderable: false,
                            searchable: false
                        }
                    ]
                });
                new $.fn.dataTable.Buttons(oTable, {
                    buttons:[
                        'copy', 'csv', 'pdf'
                    ]
                } );
                oTable.buttons().container()
                    .appendTo( $('#export-buttons') );
            }

            return {
                //main function to handle all the datatables
                init: function () {
                    handleLogsTable();
                    handleTeamsTable();
                }
            }
        }();
        jQuery(document).ready(function () {
            TableDatatables.init();
        });
    });

    function getTickets(status) {
        $("#tickets").empty();
        route = "{{ route('leader.ajax.getdata') }}";
        $.ajax({
            url: route,
            method: "POST",
            data:{
                _token: "{{ csrf_token() }}",
                getTickets: true,
                status: status,
                userID: {{ $user->id }}
            },
            dataType: 'json',
            success: function(tickets){
                console.log(tickets);

                content = `<div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Manage Tickets</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="manage_tickets" width="100%" cellspacing="0">
                                            <div id="export-buttons"></div>
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Image</th>
                                                    <th>Assigned To</th>
                                                    <th>Email</th>
                                                    <th>Project Name</th>
                                                    <th>Project Tags</th>
                                                    <th>Title</th>
                                                    <th>Labels</th>
                                                    <th>Due</th>
                                                    <th>Difficulty Level</th>
                                                    <th>Assigned At</th>
                                                    <th>Resolved At</th>
                                                </tr>
                                            </thead>
                                            <tbody>`;
                                                if(tickets.length == 0){
                                                    content += `<tr class="odd">
                                                        <td valign="top" colspan="12" class="dataTables_empty">No data available in table</td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                        <td style="display: none"></td>
                                                    </tr>`;
                                                }
                                                else {
                                                    for(i = 0; i < tickets.length; i++) {
                                                        content += `
                                                            <tr>
                                                                <td>${i+1}</td>
                                                                <td>
                                                                    <img src='{{ asset('storage') }}/${tickets[i]['member']['image']}' width='128px' alt="Image">
                                                                </td>
                                                                <td>${tickets[i]['member']['name']}</td>
                                                                <td>${tickets[i]['member']['email']}</td>
                                                                <td>${tickets[i]['project']['name']}</td>
                                                                <td>${tickets[i]['project']['tags']}</td>
                                                                <td>${tickets[i]['title']}</td>
                                                                <td>${tickets[i]['labels']}</td>
                                                                <td>${tickets[i]['due']}</td>
                                                                <td>${tickets[i]['difficulty_level']}</td>
                                                                <td>${tickets[i]['assigned_at']}</td>
                                                                <td>${tickets[i]['resolved_at']}</td>
                                                            </tr>
                                                        `;
                                                    }
                                                }
                                content += `</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>`;

                $("#tickets").append(content);
                $('#manage_tickets').dataTable();

            }
        });
    }
</script>
@endsection
