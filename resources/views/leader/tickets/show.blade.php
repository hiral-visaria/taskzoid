@extends('layouts.template')

@section('title', 'Ticket | ' . $ticket->title)

@section('content')
    @include('layouts.partials._message')
    <div class="row mt-4 pl-2">
        <div class="col-md-12 mb-4">
            <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                <div class="card-body">
                    <div class="row p-3">
                        <div class="col mr-2">
                            <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                <span>{{ $ticket->title }}</span>
                            </div>
                        </div>
                        <div class="col-md-12 mr-2">
                            <div class="pb-3">
                                @php
                                    $labels = explode(",", $ticket->labels);
                                    $colors = ["success", "info", "warning", "danger", "primary"];
                                @endphp
                                @foreach ($labels as $label)
                                    <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                @endforeach
                            </div>

                            <div class="pb-3 text-gray-600">
                                {{ $ticket->description }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="d-flex justify-content-between pb-3">
                                <div>
                                    <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                        <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                    </span>
                                    <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                        <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                    </span>
                                    @if ($ticket->status == "resolved" || $ticket->status == "approved")
                                        <span class="btn btn-outline-success btn-bold btn-sm btn-upper">
                                            <i class="fas fa-clipboard-list"></i> {{ $ticket->resolved_date }}
                                        </span>
                                    @endif
                                </div>

                                <div>
                                    <img src="{{ $ticket->member->avatar }}" alt="">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <h3>Queries</h3>

        <div class="row mr-2 mb-5">
            @foreach ($queries as $query)
                @if ($query->user->role == "leader")
                    <div class="col-md-12" style="padding-left: 45%">
                        <div class="card shadow h-100 py-2" style="border-right: dashed 2px #AAA">
                @else
                    <div class="col-md-12" style="padding-right: 45%">
                        <div class="card shadow h-100 py-2" style="border-left: dashed 2px #AAA">
                @endif
                            <div class="card-body pb-1">
                                <div class="row p-3">
                                    <div class="col-md-12 mr-2">
                                        <div class="text-lg text-gray-800 mb-1">
                                            <span>{{ $query->query }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <div class="kt-widget19__text pb-2">
                                        <i class="flaticon2-time"> </i> {{ $query->created_date }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
        </div>

        @if ($ticket->status == "assigned" || $ticket->status == "resolved")
            <div class="card-body" style="border-top: solid 1px #EEE">
                <form action="{{ route('member.query.store', [$project->id, $ticket->id]) }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <textarea name="comment" id="comment" cols="10" rows="3" class="w-100"></textarea>
                        @error('comment')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group d-flex justify-content-end">
                        <button type="submit" class="btn btn-outline-success">Submit</button>
                    </div>
                </form>
            </div>
        @endif
    </div>
@endsection

@section('styles')
    <style>
        #comment {
            padding: 10px;
            font-size: 1.1rem;
            border-radius: 5px;
            border-color: #CCC;
            resize: none;
        }

        #comment:active,
        #comment:focus {
            border: solid 1px #AAA!important;
            outline: 0;
        }
    </style>
@endsection
