<?php

namespace App\Providers;

use App\Policies\ProjectsPolicy;
use App\Policies\TeamsPolicy;
use App\Policies\TicketsPolicy;
use App\User;
use App\Policies\UsersPolicy;
use App\Project;
use App\Team;
use App\Ticket;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UsersPolicy::class,
        Project::class => ProjectsPolicy::class,
        Ticket::class => TicketsPolicy::class,
        Team::class => TeamsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
