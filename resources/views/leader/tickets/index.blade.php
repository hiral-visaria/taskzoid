@extends('layouts.template')

@section('title', 'Leader | Manage Tickets')

@section('content')
    @include('layouts.partials._message')

    <div class="row">
        <div class="col-xl-12">
            @if (auth()->user()->isLeader())
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-end">
                        <a href="{{ route('tickets.create', $project->id) }}" class="btn btn-outline-primary">
                            <i class="fa fa-plus"></i>
                            Create Ticket
                        </a>
                    </div>
                </div>
            @endif

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="unassigned-tab" data-toggle="tab" href="#unassigned" role="tab" aria-controls="unassigned" aria-selected="true">Unassigned</a>
                    <a class="nav-item nav-link" id="unresolved-tab" data-toggle="tab" href="#unresolved" role="tab" aria-controls="unresolved" aria-selected="false">Unresolved</a>
                    <a class="nav-item nav-link" id="assigned-tab" data-toggle="tab" href="#assigned" role="tab" aria-controls="assigned" aria-selected="true">Assigned</a>
                    <a class="nav-item nav-link" id="resolved-tab" data-toggle="tab" href="#resolved" role="tab" aria-controls="resolved" aria-selected="false">Resolved</a>
                    <a class="nav-item nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">Approved</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="unassigned" role="tabpanel" aria-labelledby="unassigned-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($unassignedTickets))
                            @foreach ($unassignedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1"> {{ $ticket->title }}</div>

                                                    <div>
                                                        @if (auth()->user()->isLeader())
                                                            <a href="{{ route('tickets.edit', [$project->id, $ticket->id]) }}" class="btn btn-outline-primary btn-icon mr-2"><i class="fa fa-pen"></i></a>

                                                            <a href="#" class="btn btn-outline-danger btn-icon mr-2" onclick='displayModalForm({{ $project->id }}, {{ $ticket->id }})' data-toggle='modal' data-target='#deleteModal'><i class="fa fa-trash"></i></a>

                                                            <a href="{{ route('leader.tickets.assign', [$project->id, $ticket->id]) }}" class="btn btn-outline-warning btn-icon mr-2"><i class="far fa-list-alt"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Unasigned Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade" id="unresolved" role="tabpanel" aria-labelledby="unresolved-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($unresolvedTickets))
                            @foreach ($unresolvedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('leader.tickets.show', [$project->id, $ticket->id]) }}" class="kt-widget19__username">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>

                                                    <div>
                                                        @if (auth()->user()->isLeader())
                                                            <a href="{{ route('tickets.edit', [$project->id, $ticket->id]) }}" class="btn btn-outline-primary btn-icon mr-2"><i class="fa fa-pen"></i></a>

                                                            <a href="#" class="btn btn-outline-danger btn-icon mr-2" onclick='displayModalForm({{ $project->id }}, {{ $ticket->id }})' data-toggle='modal' data-target='#deleteModal'><i class="fa fa-trash"></i></a>

                                                            <a href="{{ route('leader.tickets.assign', [$project->id, $ticket->id]) }}" class="btn btn-outline-warning btn-icon mr-2"><i class="far fa-list-alt"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                        </div>

                                                        <div>
                                                            <img src="{{ $ticket->member->avatar }}" alt="">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Unresolved Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade" id="assigned" role="tabpanel" aria-labelledby="assigned-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($assignedTickets))
                            @foreach ($assignedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('leader.tickets.show', [$project->id, $ticket->id]) }}" class="kt-widget19__username">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>

                                                    <div>
                                                        @if (auth()->user()->isLeader())
                                                            <a href="{{ route('tickets.edit', [$project->id, $ticket->id]) }}" class="btn btn-outline-primary btn-icon mr-2"><i class="fa fa-pen"></i></a>

                                                            <a href="#" class="btn btn-outline-danger btn-icon mr-2" onclick='displayModalForm({{ $project->id }}, {{ $ticket->id }})' data-toggle='modal' data-target='#deleteModal'><i class="fa fa-trash"></i></a>

                                                            <a href="{{ route('leader.tickets.assign', [$project->id, $ticket->id]) }}" class="btn btn-outline-warning btn-icon mr-2"><i class="far fa-list-alt"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                        </div>

                                                        <div>
                                                            <img src="{{ $ticket->member->avatar }}" alt="">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Asigned Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade" id="resolved" role="tabpanel" aria-labelledby="resolved-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($resolvedTickets))
                            @foreach ($resolvedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('leader.tickets.show', [$project->id, $ticket->id]) }}" class="kt-widget19__username">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>

                                                    <div>
                                                        @if (auth()->user()->isLeader())
                                                            <a href="{{ route('tickets.edit', [$project->id, $ticket->id]) }}" class="btn btn-outline-primary btn-icon mr-2"><i class="fa fa-pen"></i></a>

                                                            <a href="#" class="btn btn-outline-danger btn-icon mr-2" onclick='displayModalForm({{ $project->id }}, {{ $ticket->id }})' data-toggle='modal' data-target='#deleteModal'><i class="fa fa-trash"></i></a>

                                                            <a href="{{ route('leader.tickets.assign', [$project->id, $ticket->id]) }}" class="btn btn-outline-warning btn-icon mr-2"><i class="far fa-list-alt"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                            <span class="btn btn-outline-success btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-clipboard-list"></i> {{ $ticket->resolved_date }}
                                                            </span>
                                                        </div>

                                                        <div>
                                                            <img src="{{ $ticket->member->avatar }}" alt="">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Resolved Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($approvedTickets))
                            @foreach ($approvedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('leader.tickets.show', [$project->id, $ticket->id]) }}" class="kt-widget19__username">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>

                                                    <div>
                                                        @if (auth()->user()->isLeader())
                                                            <a href="{{ route('tickets.edit', [$project->id, $ticket->id]) }}" class="btn btn-outline-primary btn-icon mr-2"><i class="fa fa-pen"></i></a>

                                                            <a href="#" class="btn btn-outline-danger btn-icon mr-2" onclick='displayModalForm({{ $project->id }}, {{ $ticket->id }})' data-toggle='modal' data-target='#deleteModal'><i class="fa fa-trash"></i></a>

                                                            <a href="{{ route('leader.tickets.assign', [$project->id, $ticket->id]) }}" class="btn btn-outline-warning btn-icon mr-2"><i class="far fa-list-alt"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                            <span class="btn btn-outline-success btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-clipboard-list"></i> {{ $ticket->resolved_date }}
                                                            </span>
                                                        </div>

                                                        <div>
                                                            <img src="{{ $ticket->member->avatar }}" alt="">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Approved Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--DELETE MODAL-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delet Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Are you sure want to delete Ticket??</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary btn-sm m-1" data-dismiss="modal"><i class='kt-menu__link-icon flaticon2-cancel-music'></i>Close</button>
                        <button type="submit" class="btn btn-outline-danger btn-sm m-1"><i class='kt-menu__link-icon flaticon2-trash'></i>Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--END DELETE MODAL-->
@endsection
@section('styles')
    <style>
        a:hover,
        a:visited,
        a:focus{
            text-decoration: none;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script>
        function displayModalForm($projectID, $ticketID) {
            var url = '/cpanel/project/' + $projectID + '/tickets/' + $ticketID;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection
