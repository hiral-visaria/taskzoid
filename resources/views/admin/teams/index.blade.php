@extends('layouts.template')

@section('title', 'Admin | Manage Teams')

@section('content')
    @if (auth()->user()->isAdmin())
        <div class="row">
            <div class="col-md-12 d-flex justify-content-end mb-3 pr-3">
                <a href="{{ route('cpanel.teams.create') }}" class="btn btn-outline-primary">
                    <i class="fa fa-plus"></i>
                    Create Team
                </a>
            </div>
        </div>
    @endif

    @include('layouts.partials._message')

    <div class="row pl-3">
        @foreach ($teams as $team)
            <div class="col-xl-4 pl-0 mb-4">
                <div class="card shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                    <a href="{{ route('cpanel.projects.index', $team) }}" id="team_name">
                                        {{ $team->name }}
                                    </a>
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="mb-0 mr-3 font-weight-bold text-gray-600">
                                            <i class="fa fa-user"></i> {{ $team->users()->where('role', 'leader')->pluck('name')->first() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <span class="btn btn-info btn-circle btn-sm">{{ $team->projects()->count() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('styles')
    <style>
        #team_name:hover {
            text-decoration: none;
        }
    </style>
@endsection
