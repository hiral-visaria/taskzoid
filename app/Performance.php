<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    protected $fillable = [
        'member_id',
        'total_tickets',
        'approved_tickets',
        'unresolved_tickets',
        'currently_assigned_tickets'
    ];

    public function member() {
        return $this->belongsTo(User::class, 'member_id');
    }
}
