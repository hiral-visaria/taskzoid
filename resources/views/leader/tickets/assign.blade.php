@extends('layouts.template')

@section('title', 'Leader | Assign Ticket')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <!-- CARD HEADER -->
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-plus"></i> Assign Ticket
                </h6>
            </div>
            <!-- END OF CARD HEADER -->

            <!-- CARD BODY -->
            <div class="card-body">
                <form class="kt-form" action="{{ route('leader.tickets.assign-ticket', [$project->id, $ticket->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="member_id">Member</label>
                            <select name="member_id" id="member_id" class="form-control select2">
                                <option value="" selected disabled>Select Member</option>
                                @foreach ($members as $member)
                                    <option value="{{ $member->id }}" {{ old('member_id') == $member->id ? 'selected' : '' }}>{{ $member->name }}</option>
                                @endforeach
                            </select>
                            @error('member_id')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" id="automatic_assignment" onchange="automaticAssignment({{ auth()->user()->team_id }})"> Automatic Assignment
                                <span></span>
                            </label>
                        </div>

                        <div class="form-group col-md-12 text-muted" id="assigned_to"></div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary btn-sm m-1"><i class='fa fa-pen'></i> Assign</button>
                </form>
            </div>
            <!-- END OF CARD BODY -->
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $('#member_id').select2();

        function automaticAssignment(teamID) {
            if($("#automatic_assignment").is(':checked')) {
                $("#member_id").attr('disabled', true);

                route = "{{ route('leader.ajax.getdata') }}";
                $.ajax({
                    url: route,
                    method: "POST",
                    data:{
                        _token: "{{ csrf_token() }}",
                        getBestOrAvailableMember: true,
                        teamID: teamID
                    },
                    dataType: 'json',
                    success: function(member){
                        console.log(member);

                        eligibleMember = `
                            <input type="hidden" name="member_id" value=${member.id}>
                            <span>Assigning To : ${member.name}</span>
                        `;

                        $("#assigned_to").append(eligibleMember);
                    }
                });
            } else {
                $("#assigned_to").empty();
                $("#member_id").attr('disabled', false);
            }
        }
    </script>
@endsection
