<?php

namespace App\Http\Controllers\leader;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tickets\CreateTicketRequest;
use App\Http\Requests\Tickets\StatusUpdateRequest;
use App\Http\Requests\Tickets\UpdateTicketRequest;
use App\Notifications\TicketStatusNotification;
use App\Project;
use App\Query;
use App\Ticket;
use App\User;
use CreateTicketsTable;
use Illuminate\Http\Request;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        $this->authorize('create', Ticket::class);

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.tickets.create', compact([
            'project',
            'notifications'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTicketRequest $request, Project $project)
    {
        if($request->status == "assigned") {
            $assignedAt = now();
            $memberID = $request->member_id;
        } else {
            $assignedAt = null;
            $memberID = null;
        }

        $ticket = Ticket::create([
            'project_id' => $project->id,
            'member_id' => $memberID,
            'title' => $request->title,
            'description' => $request->description,
            'labels' => $request->labels,
            'due' => $request->due,
            'difficulty_level' => $request->difficulty_level,
            'status' => $request->status,
            'assigned_at' => $assignedAt
        ]);

        if($request->status == "assigned") {
            $member = User::where('id', $request->member_id)->first();
            $member->performance->increment('currently_assigned_tickets');
            $member->performance->increment('total_tickets');

            $member = User::where('id', $request->member_id)->first();
            $member->notify(new TicketStatusNotification($ticket, "assigned"));
        }

        session()->flash('success', "Ticket Created Successfully");
        return redirect(route('leader.projects.show'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Ticket $ticket) {
        $this->authorize('view', Ticket::class);

        $queries = Query::where('ticket_id', $ticket->id)->with('user')->get();

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.tickets.show', compact([
            'project',
            'ticket',
            'queries',
            'notifications'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Ticket $ticket)
    {
        $this->authorize('update', $ticket);

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.tickets.edit', compact([
            'ticket',
            'project',
            'notifications'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTicketRequest $request, Project $project, Ticket $ticket)
    {
        $this->authorize('update', $ticket);
        $data = $request->only(['title', 'description', 'labels', 'due', 'difficulty_level', 'status']);

        $member = User::where('id', $request->member_id)->first();
        $currently_assigned_to = User::where('id', $ticket->member_id)->first();

        if($request->status == "assigned") {
            if($request->member_id != $ticket->member_id) {
                $currently_assigned_to->performance->decrement('currently_assigned_tickets');

                $member->performance->increment('currently_assigned_tickets');
                $member->performance->increment('total_tickets');
            }
        } else {
            if($member->performance->currently_assigned_tickets == 0 || $member->performance->total_tickets == 0) {
                session()->flash('error', "Updation of Ticket was Unsuccessfully");
                return redirect(route('leader.projects.show'));
            }

            $member->performance->decrement('currently_assigned_tickets');
            $member->performance->decrement('total_tickets');
        }

        if($request->has("member_id")) {
            if($request->member_id != $ticket->member_id) {
                $data['assigned_at'] = now();
                $data['member_id'] = $request->member_id;
            }
        } else {
            $data['assigned_at'] = null;
            $data['member_id'] = null;
        }

        $ticket->update($data);

        if($request->has("member_id"))
        {
            $member = User::where('id', $request->member_id)->first();
            $member->notify(new TicketStatusNotification($ticket, "assigned"));
        }

        session()->flash('success', "Ticket Updated Successfully");
        return redirect(route('leader.projects.show', compact([
            'project'
        ])));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Ticket $ticket)
    {
        $this->authorize('delete', $ticket);

        if($ticket->status == "assigned") {
            $member = User::where('id', $ticket->member_id)->first();
            if($member->performance->currently_assigned_tickets == 0 || $member->performance->total_tickets == 0) {
                session()->flash('error', "Deletion of Ticket was Unsuccessfully");
                return redirect(route('leader.projects.show'));
            }

            $member->performance->decrement('currently_assigned_tickets');
            $member->performance->decrement('total_tickets');
        }

        $ticket->delete();

        session()->flash('success', "Ticket Deleted Successfully");
        return redirect(route('leader.projects.show', compact([
            'project'
        ])));
    }

    public function status(Project $project, Ticket $ticket)
    {
        $this->authorize('statusUpdate', $ticket);

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.tickets.status', compact([
            'project',
            'ticket',
            'notifications'
        ]));
    }

    public function statusUpdate(StatusUpdateRequest $request, Project $project, Ticket $ticket)
    {
        $this->authorize('statusUpdate', $ticket);
        if($request->status != "approved") {
            $member = User::where('id', $request->member_id)->first();
            if($request->member_id != $ticket->member_id) {
                // reassinged to someone else
                $member->performance->increment('currently_assigned_tickets');
                $member->performance->increment('total_tickets');

                $ticket->update([
                    'member_id' => $request->member_id,
                    'status' => $request->status,
                    'resolved_at' => null
                ]);
            } else {
                // reassigned to same member
                $ticket->update([
                    'status' => $request->status,
                    'resolved_at' => null
                ]);
            }
        } else {
            // approved
            $member = User::where('id', $ticket->member_id)->first();
            if($member->performance->currently_assigned_tickets == 0) {
                session()->flash('error', "Updation of Ticket was Unsuccessfully");
                return redirect(route('leader.projects.show'));
            }

            $member->performance->decrement('currently_assigned_tickets');
            $member->performance->increment('approved_tickets');
            $member->notify(new TicketStatusNotification($ticket, 'approved'));

            $ticket->update([
                'status' => $request->status
            ]);
        }

        session()->flash('success', "Ticket Updated Successfully");
        return redirect(route('leader.projects.show', compact([
            'project'
        ])));
    }

    public function assign(Project $project, Ticket $ticket)
    {
        $this->authorize('statusUpdate', $ticket);
        $members = User::where('team_id', $project->team_id)->where('role', 'member')->get();

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.tickets.assign', compact([
            'project',
            'ticket',
            'members',
            'notifications'
        ]));
    }

    public function assignTicket(Request $request, Project $project, Ticket $ticket)
    {
        $this->authorize('statusUpdate', $ticket);
        $member = User::where('id', $request->member_id)->first();

        // for unassign to assign
        if($ticket->member_id == NULL) {
            $member->performance->increment('total_tickets');

            $ticket->update([
                'member_id' => $request->member_id,
                'status' => "assigned",
                'assigned_at' => now()
            ]);
        } else {
            // unresolved to assign - reassigning

            // assigning to different person
            if($request->member_id != $ticket->member_id) {
                $member->performance->increment('total_tickets');
            } else {
                // assigning to same person who unresolved it
                $member->performance->decrement('unresolved_tickets');
            }

            $member->performance->increment('currently_assigned_tickets');

            $ticket->update([
                'status' => "assigned",
                'assigned_at' => now()
            ]);
        }

        $member->performance->increment('currently_assigned_tickets');
        $member->notify(new TicketStatusNotification($ticket, "assigned"));

        session()->flash('success', "Ticket Assigned Successfully");
        return redirect(route('leader.projects.show', compact([
            'project'
        ])));
    }
}
