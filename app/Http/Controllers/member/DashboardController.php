<?php

namespace App\Http\Controllers\member;

use App\Project;
use App\User;
use App\Http\Controllers\Controller;
use App\Log;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(User $member = null) {
        if($member == null){
            $member = User::where('id', auth()->user()->id)->first();
        }
        $projects = Project::whereIn('id', auth()->user()->tickets->pluck('project_id'))->get();

        $performance = DB::select(DB::raw("SELECT member_id, ((approved_tickets - unresolved_tickets) / total_tickets) * 100 as ticketCalculation FROM `performances` p1 WHERE currently_assigned_tickets in (SELECT MIN(`currently_assigned_tickets`) FROM `performances` WHERE `member_id` = 12) AND `member_id` = 12 ORDER BY ticketCalculation DESC"));

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('member.dashboard', compact([
            'member',
            'projects',
            'performance',
            'notifications'
        ]));
    }

    public function getLogs() {
        return \DataTables::of(Log::where('member_id', auth()->user()->id)->get())
            ->addIndexColumn()
            ->make(true);
    }
}
