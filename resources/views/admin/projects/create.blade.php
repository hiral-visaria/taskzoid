@extends('layouts.template')

@section('title', 'Admin | Create Project')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- CARD HEADER -->
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-plus"></i> Add Project
                    </h6>
                </div>
                <!-- END OF CARD HEADER -->

                <!-- CARD BODY -->
                <div class="card-body">
                    <form class="kt-form" action="{{ route('cpanel.projects.store', $team->id) }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Name</label>
                                <input type="text" placeholder="Enter name" value="{{old('name')}}"
                                        class="form-control @error('name') is-invalid @enderror"
                                        name="name" id="name">
                                @error('name')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label>Description</label>
                                <textarea class="form-control" id="description" name="description" placeholder="Enter Description">{{ old('description') }}</textarea>
                                @error('description')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label>Tags</label>
                                <input type="text" placeholder="Eg. c, java, etc" value="{{old('tags')}}"
                                        class="form-control @error('tags') is-invalid @enderror"
                                        name="tags" id="tags">
                                @error('tags')
                                    <p class="text-danger">{{ $message }}</p>
                                @else
                                    <span class="text-danger">More than one label should be entered as comma separated values</span>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label for="due">Due</label>
                                <input type="text" value="{{old('due')}}" class="form-control" name="due" id="due">
                                @error('due')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-outline-primary btn-sm m-1"><i class='fa fa-plus'></i> Add</button>
                    </form>
                </div>
                <!-- END OF CARD BODY -->
            </div>
        </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr("#due", {
            enableTime: true,
            minDate: "today"
        });

        $('.select2').select2();
    </script>
@endsection
