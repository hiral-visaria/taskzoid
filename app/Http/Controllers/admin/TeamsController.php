<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Teams\CreateTeamRequest;
use App\Team;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function index()
    {
        $this->authorize('view', Team::class);
        $teams = Team::where('deleted_at', null)->get();

        return view('admin.teams.index', compact([
            'teams'
        ]));
    }

    public function show(Team $team) {
        return view('admin.team-members.index', compact([
            'team'
        ]));
    }

    public function create() {
        $this->authorize('create', Team::class);
        return view('admin.teams.create');
    }

    public function store(CreateTeamRequest $request)
    {
        $team = Team::create([
            'name' => $request->name
        ]);

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);

        session()->flash('success', "Team Created Successfully");
        return redirect(route('cpanel.teams.index'));
    }
}
