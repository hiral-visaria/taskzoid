<?php

namespace App\Http\Controllers\leader;

use App\Http\Controllers\Controller;
use App\Notifications\QueryNotification;
use App\Project;
use App\Query;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class QueriesController extends Controller
{
    public function store(Request $request, Project $project, Ticket $ticket)
    {
        $query = Query::create([
            'ticket_id' => $ticket->id,
            'member_id' => auth()->user()->id,
            'query' => $request->comment
        ]);

        $queries = Query::where('ticket_id', $ticket->id)->with('user')->get();

        $member = User::where('id', $ticket->member_id)->first();
        $member->notify(new QueryNotification($project, $ticket, $query));

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return redirect()->back();
    }
}
