@extends('layouts.template')

@section('title', 'Leader | Status Update')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <!-- CARD HEADER -->
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-pen"></i> Status Update
                </h6>
            </div>
            <!-- END OF CARD HEADER -->

            <!-- CARD BODY -->
            <div class="card-body">
                <form class="kt-form" action="{{ route('leader.tickets.status-update', [$project->id, $ticket->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="status form-control select2" onchange="getMembers({{ auth()->user()->team_id }})">
                                <option disabled selected>Select Status</option>
                                <option value="approved" @if(old('status', $ticket->status) == "approved") selected @endif>Approve</option>
                                <option value="assigned"  @if(old('status', $ticket->status) == "assigned") selected @endif>Reassign</option>
                            </select>
                            @error('status')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group col-md-12" id="members"></div>

                        <div class="form-group col-md-12 d-none" id="assignment">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox" id="automatic_assignment" onchange="automaticAssignment({{ auth()->user()->team_id }})"> Automatic Assignment
                                <span></span>
                            </label>
                        </div>

                        <div class="form-group col-md-12 text-muted" id="assigned_to"></div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary btn-sm m-1"><i class='fa fa-pen'></i> Update</button>
                </form>
            </div>
            <!-- END OF CARD BODY -->
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        if(window.location.reload) {
            getMembers({{ auth()->user()->team_id }});
        }

        function getMembers(teamID)
        {
            status = $('.status option:selected').val();
            console.log(teamID);
            if(status == "approved") {
                console.log(status);
                $('#members').empty();
                $('#assignment').remove();
            }else{
                console.log(status);
                route = "{{ route('leader.ajax.getdata') }}";
                oldMemberID = "{{ old('member_id') }}";
                $.ajax({
                    url: route,
                    method: "POST",
                    data:{
                        _token: "{{ csrf_token() }}",
                        getMembers: true,
                        teamID: teamID
                    },
                    dataType: 'json',
                    success: function(members){
                        console.log(members);

                        $('#members').empty();

                        teamMembers = `
                            <label for="member_id">Member</label>
                            <select name="member_id" id="member_id" class="form-control">
                                <option disabled selected>Select Member</option>
                        `;

                        for(i = 0; i < members.length; i++) {
                            if(oldMemberID == members[i]['id'])
                                teamMembers += `<option value='${members[i]['id']}' selected>${members[i]['name']}</option>`;
                            else
                                teamMembers += `<option value='${members[i]['id']}'>${members[i]['name']}</option>`;
                        }
                        teamMembers += `
                            </select>
                            @error('member_id')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>`;

                        $("#members").append(teamMembers);
                        $("#member_id").select2();
                        $("#assignment").removeClass("d-none");
                    }
                });
            }
        }

        function automaticAssignment(teamID) {
            if($("#automatic_assignment").is(':checked')) {
                $("#member_id").attr('disabled', true);

                route = "{{ route('leader.ajax.getdata') }}";
                $.ajax({
                    url: route,
                    method: "POST",
                    data:{
                        _token: "{{ csrf_token() }}",
                        getBestOrAvailableMember: true,
                        teamID: teamID
                    },
                    dataType: 'json',
                    success: function(member){
                        console.log(member);

                        eligibleMember = `
                            <input type="hidden" name="member_id" value=${member.id}>
                            <span>Assigning To : ${member.name}</span>
                        `;

                        $("#assigned_to").append(eligibleMember);
                    }
                });
            } else {
                $("#assigned_to").empty();
                $("#member_id").attr('disabled', false);
            }
        }

        $('.select2').select2();
    </script>
@endsection
