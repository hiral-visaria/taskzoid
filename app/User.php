<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id',
        'name',
        'email',
        'password',
        'role',
        'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * HELPER METHODS
     */
    public function isAdmin(){
        return $this->role === 'admin';
    }

    public function isLeader(){
        return $this->role === 'leader';
    }

    public function isMember(){
        return $this->role === 'member';
    }

    public function isNaive(){
        return $this->role === 'naive';
    }

    public function deleteImage() {
        Storage::delete($this->image);
    }

    public function percentage($percentage) {
        if($percentage <= 30)
            return "<span class='text-danger font-weight-bold'>$percentage%</span>";
        else if ($percentage > 30 && $percentage < 75)
            return "<span class='text-warning font-weight-bold'>$percentage%</span>";

        return "<span class='text-success font-weight-bold'>$percentage%</span>";
    }

    /**
     * ACCESSORS
     */
    public function getAvatarAttribute(){
        $size = 36;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}&color=22b9ff&background=rgba(34,%20185,%20255,%200.1)";
    }

    /**
     * RELATIONSHIP FUNCIONS
     */
    public function team() {
        return $this->belongsTo(Team::class);
    }

    public function tickets() {
        return $this->hasMany(Ticket::class, 'member_id');
    }

    public function queries() {
        return $this->hasMany(Query::class, 'member_id');
    }

    public function performance() {
        return $this->hasOne(Performance::class, 'member_id');
    }

    public function logs() {
        return $this->hasMany(Log::class, 'member_id');
    }
}
