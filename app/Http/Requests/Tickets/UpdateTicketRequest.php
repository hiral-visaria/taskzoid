<?php

namespace App\Http\Requests\Tickets;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_id' => 'exists:users,id',
            'title' => 'required|max:255',
            'labels' => 'max:255',
            'due' => 'required',
            'difficulty_level' => 'required',
            'status' => 'required'
        ];
    }
}
