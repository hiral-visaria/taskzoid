<?php

namespace App\Notifications;

use App\Project;
use App\Query;
use App\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class QueryNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project, Ticket $ticket, Query $query)
    {
        $this->project = $project;
        $this->ticket = $ticket;
        $this->query = $query;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line("New Query / Comment")
                    ->line("Project Name : {$this->project->name}")
                    ->line("Ticket: {$this->ticket->title}")
                    ->line("Due: {$this->ticket->due_date}")
                    ->line("Assigned To: {$this->ticket->assigned_at}")
                    ->line("Query: {$this->query->query}")
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'project' => $this->project,
            'ticket' => $this->ticket,
            'query' => $this->query
        ];
    }
}
