@extends('layouts.template')

@section('title', 'Leader | Manage Project')

@section('content')
        @if (auth()->user()->isAdmin())
            <div class="row">
                <div class="col-md-12 d-flex justify-content-end mb-3 pr-3">
                    <a href="{{ route('cpanel.projects.create', $team->id) }}" class="btn btn-outline-primary">
                        <i class="la la-plus"></i>
                        Create Project
                    </a>
                </div>
            </div>
        @endif

        @include('layouts.partials._message')

        <div class="row pl-3">
            @if (sizeof($projects))
                @foreach ($projects as $project)
                    <div class="col-xl-4 pl-0 mb-4">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-gray-600 text-uppercase mb-1">
                                            <a href="@if(auth()->user()->isAdmin()) {{ route('cpanel.projects.show', $project->id)  }}@else {{ route('leader.projects.show', $project->id) }} @endif" class="text-gray-600" id="project_name">
                                                {{ $project->name }}
                                            </a>
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="mb-0 mr-3 font-weight-bold text-gray-600">
                                                    @php
                                                        $tags = explode(",", $project->tags);
                                                        $colors = ["success", "info", "warning", "danger", "primary"];
                                                    @endphp
                                                    @foreach ($tags as $tag)
                                                        <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $tag }}</span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <span class="btn btn-outline-danger">{{ $project->due_date }}</span>
                                    </div>
                                </div>
                                <div class="row no-gutters align-items-center mt-3">
                                    {{ $project->description }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else

            @endif
        </div>
@endsection

@section('styles')
    <style>
        #project_name:hover,
        #project_name:visited,
        #project_name:focus {
            text-decoration: none;
        }
    </style>
@endsection
