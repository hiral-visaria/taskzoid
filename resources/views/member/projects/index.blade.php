@extends('layouts.template')

@section('title', 'Member | Manage Projects')

@section('content')
    @include('layouts.partials._message')

    <div class="row">
        @foreach ($projectsWithTicketsForMember as $project)
            <div class="col-xl-4 pl-0 mb-4">
                <div class="card shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-gray-600 text-uppercase mb-1">
                                    <a href="{{ route('member.projects.show', $project->id) }}" id="project_name">
                                        {{ $project->name }}
                                    </a>
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="mb-0 mr-3 font-weight-bold text-gray-600">
                                            @php
                                                $tags = explode(",", $project->tags);
                                                $colors = ["success", "info", "warning", "danger", "primary"];
                                            @endphp
                                            @foreach ($tags as $tag)
                                                <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $tag }}</span>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <span class="btn btn-outline-danger">{{ $project->due_date }}</span>
                            </div>
                        </div>
                        <div class="row no-gutters align-items-center mt-3">
                            {{ $project->description }}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('styles')
    <style>
        #project_name:hover,
        #project_name:visited,
        #project_name:focus {
            text-decoration: none;
        }
    </style>
@endsection
