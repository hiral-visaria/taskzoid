@extends('layouts.template')

@section('title', 'Member | Edit Details')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <!-- CARD HEADER -->
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-pen"></i> Edit Member
                </h6>
            </div>
            <!-- END OF CARD HEADER -->

            <!-- CARD BODY -->
            <div class="card-body">
                <form class="kt-form" action="{{ route('member.update', $member->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Name</label>
                            <input type="text" placeholder="Enter name" value="{{old('name', $member->name)}}"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name" id="name">
                            @error('name')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label>Email</label>
                            <input type="email" placeholder="Enter email" value="{{old('email', $member->email)}}"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email" id="email">
                            @error('email')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            @if($member->image == null)
                                <img src="{{ asset("storage/users/neutral.png") }}" class="rounded" width="128px" alt="image">
                            @else
                                <img src="{{ asset("storage/" . $member->image) }}" class="rounded" width="128px" alt="image">
                            @endif
                        </div>
                        <div class="form-group col-md-12">
                            <label for="image">Image</label>
                            <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" id="image">
                            @error('image')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary btn-sm m-1"><i class='fa fa-plus'></i> Update</button>
                </form>
            </div>
            <!-- END OF CARD BODY -->
        </div>
    </div>
</div>
@endsection
