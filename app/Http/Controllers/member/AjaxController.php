<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function getData(Request $request){
        if($request->get('getTickets')){
            $tickets = Ticket::where('member_id', $request->memberID)->where('status', $request->status)->with('project')->get();
            return json_encode($tickets);
        }
    }
}
