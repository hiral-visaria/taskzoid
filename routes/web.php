<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    // begin: admin ke route
    Route::get('/cpanel/dashboard', 'admin\DashboardController@index')->name('cpanel.dashboard');
    Route::get('/cpanel/dashboard/getlogs', 'admin\DashboardController@getLogs')->name('cpanel.datatable.logs');
    Route::get('/cpanel/dashboard/getteams', 'admin\DashboardController@getTeams')->name('cpanel.datatable.teams');
    Route::get('/cpanel/dashboard/{team}/getmembers', 'admin\DashboardController@getMembers')->name('cpanel.datatable.members');
    Route::get('/cpanel/member/{member}/dashboard', 'member\DashboardController@index')->name('cpanel.member.dashboard');

    Route::resource('/cpanel/users', 'admin\UsersController');
    Route::get('/cpanel/users/{user}/addToTeam', 'admin\UsersController@addToTeamView')->name('cpanel.users.addToTeamView');
    Route::put('/cpanel/users/{user}/addToTeam', 'admin\UsersController@addToTeam')->name('cpanel.users.addToTeam');
    Route::get('/cpanel/datatable/getusers', 'admin\UsersController@getUsers')->name('cpanel.datatable.users');

    Route::get('/cpanel/teams', 'admin\TeamsController@index')->name('cpanel.teams.index');
    Route::get('/cpanel/teams/{team}', 'admin\TeamsController@show')->name('cpanel.teams.show');
    Route::get('/cpanel/team/create', 'admin\TeamsController@create')->name('cpanel.teams.create');
    Route::post('/cpanel/teams', 'admin\TeamsController@store')->name('cpanel.teams.store');

    Route::get('/cpanel/team/{team}/projects/create', 'admin\ProjectsController@create')->name('cpanel.projects.create');
    Route::post('/cpanel/team/{team}/projects', 'admin\ProjectsController@store')->name('cpanel.projects.store');
    Route::get('/cpanel/team/{team}/projects', 'leader\ProjectsController@index')->name('cpanel.projects.index');
    Route::get('/cpanel/project/{project}', 'leader\ProjectsController@show')->name('cpanel.projects.show');

    Route::get('/cpanel/{user}/edit', 'admin\AdminsController@edit')->name('cpanel.edit');
    Route::put('/cpanel/{user}', 'admin\AdminsController@update')->name('cpanel.update');
    // end: admin ke route

    // begin: leaders ke route
    Route::get('/leader/dashboard', 'leader\DashboardController@index')->name('leader.dashboard');
    Route::get('/leader/dashboard/getlogs', 'leader\DashboardController@getLogs')->name('leader.datatable.logs');
    Route::get('/leader/dashboard/getmembers', 'leader\DashboardController@getMembers')->name('leader.datatable.members');
    Route::get('/leader/member/{member}/dashboard', 'member\DashboardController@index')->name('leader.member.dashboard');

    Route::get('/leader/projects', 'leader\ProjectsController@index')->name('leader.projects.index');
    Route::get('/leader/project/{project}', 'leader\ProjectsController@show')->name('leader.projects.show');
    Route::resource('/leader/project/{project}/tickets', 'leader\TicketsController');
    Route::get('/leader/project/{project}/ticket/{ticket}', 'leader\TicketsController@show')->name('leader.tickets.show');
    Route::post('/leader/project/{project}/ticket/{ticket}/query', 'leader\QueriesController@store')->name('leader.query.store');
    Route::get('/leader/project/{project}/ticket/{ticket}/status', 'leader\TicketsController@status')->name('leader.tickets.status');
    Route::put('/leader/project/{project}/ticket/{ticket}/status', 'leader\TicketsController@statusUpdate')->name('leader.tickets.status-update');
    Route::get('/leader/project/{project}/ticket/{ticket}/assign', 'leader\TicketsController@assign')->name('leader.tickets.assign');
    Route::put('/leader/project/{project}/ticket/{ticket}/assign-ticket', 'leader\TicketsController@assignTicket')->name('leader.tickets.assign-ticket');

    Route::post('/ajax', 'leader\AjaxController@getData')->name('leader.ajax.getdata');

    Route::get('/leader/{user}/edit', 'leader\LeadersController@edit')->name('leader.edit');
    Route::put('/leader/{user}', 'leader\LeadersController@update')->name('leader.update');
    // end: leaders ke route

    // begin: members ke route
    Route::get('/members/dashboard', 'member\DashboardController@index')->name('member.dashboard');
    Route::get('/members/dashboard/getlogs', 'member\DashboardController@getLogs')->name('member.datatable.logs');
    Route::post('/member/ajax', 'member\AjaxController@getData')->name('member.ajax.getdata');
    Route::get('/members/projects', 'member\ProjectsController@index')->name('member.projects.index');
    Route::get('/members/project/{project}', 'member\ProjectsController@show')->name('member.projects.show');
    Route::get('/members/project/{project}/ticket/{ticket}', 'member\TicketsController@show')->name('member.tickets.show');
    Route::post('/members/project/{project}/ticket/{ticket}/query', 'member\QueriesController@store')->name('member.query.store');
    Route::put('/member/project/{project}/ticket/{ticket}/resolve', 'member\TicketsController@resolve')->name('member.tickets.resolve');
    Route::put('/member/project/{project}/ticket/{ticket}/unresolve', 'member\TicketsController@unresolve')->name('member.tickets.unresolve');

    Route::get('/member/{member}/edit', 'member\MembersController@edit')->name('member.edit');
    Route::put('/member/{member}', 'member\MembersController@update')->name('member.update');
    // end: members ke route
});
