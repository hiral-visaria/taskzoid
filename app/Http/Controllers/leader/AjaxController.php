<?php

namespace App\Http\Controllers\leader;

use App\Http\Controllers\Controller;
use App\Performance;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function getData(Request $request){
        if($request->get('getMembers')) {
            $members = User::where('team_id', $request->teamID)->where('role', 'member')->get();
            return json_encode($members);

        } else if ($request->get('getBestOrAvailableMember')) {
            // finding the avaibale member
            $membersID = User::where('team_id', $request->teamID)->where('role', 'member')->pluck('id')->toArray();

            $membersEligibleForAssigningTask = DB::select(DB::raw("SELECT member_id, ((approved_tickets - unresolved_tickets) / total_tickets) as ticketCalculation FROM `performances` p1 WHERE currently_assigned_tickets in (SELECT MIN(`currently_assigned_tickets`) FROM `performances` WHERE `member_id` in (" . implode(', ', $membersID). ")) AND `member_id` in (" . implode(', ', $membersID). ") ORDER BY ticketCalculation DESC"));

            if(sizeof($membersEligibleForAssigningTask) > 1)
            {
                $free_members_id = array();
                $i = 0;
                foreach($membersEligibleForAssigningTask as $memberEligibleForAssigningTask) {
                    $free_members_id[$i++] = $memberEligibleForAssigningTask->member_id;
                }

                $bestMemberForAssigningTask = DB::select(DB::raw("SELECT member_id, sum((resolved_at - assigned_at)) as time_constraint FROM `tickets` WHERE `status` in ('resolved', 'approved') AND member_id in (" . implode(', ', $free_members_id). ") GROUP BY member_id ORDER BY time_constraint LIMIT 1"));

                if(sizeof($bestMemberForAssigningTask))
                    $eligibleMember = User::where('id', $bestMemberForAssigningTask[0]->member_id)->first();
                else
                    $eligibleMember = User::where('id', $free_members_id[0])->first();
            } else {
                $eligibleMember = User::where('id', $membersEligibleForAssigningTask[0]->member_id)->first();
            }

            return json_encode($eligibleMember);
        } else if($request->get('getTickets')){
            $team_id = User::where('id', $request->userID)->pluck('team_id')->first();
            $member_id = User::where('team_id', $team_id)->where('role', 'member')->pluck('id')->toArray();
            $tickets = Ticket::whereIn('member_id', $member_id)->where('status', $request->status)->with(['project', 'member'])->get();
            return json_encode($tickets);
        }
    }
}
