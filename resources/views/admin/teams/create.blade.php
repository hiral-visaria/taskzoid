@extends('layouts.template')

@section('title', 'Admin | Create Team')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <!-- CARD HEADER -->
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-plus"></i> Add Team
                </h6>
            </div>
            <!-- END OF CARD HEADER -->

            <!-- CARD BODY -->
            <div class="card-body">
                <form class="kt-form" action="{{ route('cpanel.teams.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Name</label>
                            <input type="text" placeholder="Enter name" value="{{old('name')}}"
                                    class="form-control @error('name') is-invalid @enderror"
                                    name="name" id="name">
                            @error('name')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary btn-sm m-1"><i class='fa fa-plus'></i> Add</button>
                </form>
            </div>
            <!-- END OF CARD BODY -->
        </div>
    </div>
</div>
@endsection
