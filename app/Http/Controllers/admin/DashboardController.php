<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Log;
use App\Project;
use App\Team;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(){
        $user = User::where('id', auth()->user()->id)->first();
        $teams = Team::all();

        $bestMember = DB::select(DB::raw("SELECT member_id, users.*, (((approved_tickets - unresolved_tickets) / total_tickets) * 100) as ticketCalculation FROM `performances`, users WHERE member_id = users.id AND role = 'member' ORDER BY ticketCalculation DESC LIMIT 1"));

        $teamPerformance = array();
        $i = 0;
        foreach($teams as $team)
        {
            $membersID = User::where('team_id', $team->id)->where('role', 'member')->pluck('id')->toArray();

            if($membersID) {
                $teamPerformance[$i++] = DB::select(DB::raw("SELECT sum(((approved_tickets - unresolved_tickets) / total_tickets) * 100) / " . sizeof($membersID) . " as ticketCalculation FROM `performances` WHERE `member_id` in (" . implode(', ', $membersID). ")"));
            }
        }

        $bestMemberTeam = Team::where('id', $bestMember[0]->team_id)->first();
        $bestMemberTickets = Ticket::where('member_id', $bestMember[0]->id)->pluck('project_id');
        $bestMemberProjects = Project::whereIn('id', $bestMemberTickets)->get();

        return view('admin.dashboard', compact([
            'user',
            'bestMember',
            'bestMemberTeam',
            'bestMemberTickets',
            'bestMemberProjects'
        ]));
    }

    public function getTeams()
    {
        $teams = Team::with(['users' => function($query){
            $query->where('role', 'leader');
        }])->get();

        return \DataTables::of($teams)
                ->addIndexColumn()
                ->addColumn('name', function (Team $team) {
                    return $team->name;
                })
                ->addColumn('team_leader', function (Team $team) {
                    if(sizeof($team->users)) {
                        return $team->users[0]->name;
                    }
                })
                ->addColumn('projects', function (Team $team) {
                    return $team->projects()->count();
                })
                ->addColumn('tickets', function (Team $team) {
                    $projectID = Project::where('team_id', $team->id)->pluck('id');
                    $ticketsCount = Ticket::whereIn('project_id', $projectID)->count();
                    return $ticketsCount;
                })
                ->addColumn('performance', function (Team $team) {
                    $membersID = User::where('team_id',$team->id)->where('role', 'member')->pluck('id')->toArray();

                    if($membersID) {
                        $performance = DB::select(DB::raw("SELECT sum(((approved_tickets - unresolved_tickets) / total_tickets) * 100) / " . sizeof($membersID) . " as ticketCalculation FROM `performances` p1 WHERE `member_id` in (" . implode(', ', $membersID). ") ORDER BY ticketCalculation DESC"));
                    } else {
                        $performance = array();
                    }
                    return sizeof($performance) ? $team->performance(round($performance[0]->ticketCalculation, 2)) : $team->performance(0);
                })
                ->addColumn('actions', function (Team $team) {
                    $actionButtons = "<div class='d-flex'>";

                    if (auth()->user()->can('view', Project::class)) {
                        $actionButtons .= "<a href='" .  route('cpanel.projects.index', $team->id) . "' class='btn btn-icon btn-outline-success mr-2'><i class='fa fa-eye'></i></a>";
                    }

                    if (auth()->user()->can('view', User::class)) {
                        $actionButtons .= "<a href='" . route('cpanel.teams.show', $team->id) . "' class='btn btn-icon btn-outline-info mr-1'><i class='fa fa-user'></i></a>";
                    }
                    return $actionButtons . '</div>';
                })
                ->rawColumns(['performance', 'actions'])
                ->make(true);
    }

    public function getMembers(Team $team) {
        $users = User::where('team_id', $team->id)->where('role', 'member')->get();
        if($users) {
            return \DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('image', function (User $user) {
                    if ($user->image)
                        return '<img src=' . asset("storage/" . $user->image) . ' alt="Student Image" width="128">';
                    else
                        return '<img src=' . asset("storage/users/neutral.png") . ' alt="Student Image" width="128">';
                })
                ->addColumn('performance', function (User $user) {
                    $performance = DB::select(DB::raw("SELECT member_id, ((approved_tickets - unresolved_tickets) / total_tickets) * 100 as ticketCalculation FROM `performances` p1 WHERE currently_assigned_tickets in (SELECT MIN(`currently_assigned_tickets`) FROM `performances` WHERE `member_id` = $user->id) AND `member_id` = $user->id ORDER BY ticketCalculation DESC"));
                    return sizeof($performance) ? $user->percentage(round($performance[0]->ticketCalculation, 2)) : $user->percentage(0);
                })
                ->addColumn('actions', function (User $user) {
                    $actionButtons = "<div class='d-flex'>";

                    if (auth()->user()->can('view', User::class)) {
                        $actionButtons .= "<a href='" . route('cpanel.member.dashboard', $user->id) . "' class='btn btn-icon btn-outline-success mr-1'><i class='fa fa-eye'></i></a>";
                    }
                    return $actionButtons . '</div>';
                })
                ->rawColumns(['image', 'performance', 'actions'])
                ->make(true);
        }
    }

    public function getLogs() {
        return \DataTables::of(Log::where('member_id', auth()->user()->id)->get())
            ->addIndexColumn()
            ->make(true);
    }
}
