<?php

use App\Performance;
use Illuminate\Database\Seeder;

class PerformanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 12; $i <= 43; $i++)
        {
            $performance = new Performance();
            $performance->member_id = $i;
            $performance->total_tickets = 2;
            $performance->approved_tickets = rand(0, 1);
            $performance->unresolved_tickets = rand(0, 1);
            $performance->currently_assigned_tickets = rand(0, 1); 

            $performance->save();
        }
    }
}
