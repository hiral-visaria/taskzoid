<?php

use App\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::create(['name' => 'Team A']);
        Team::create(['name' => 'Team B']);
        Team::create(['name' => 'Team C']);
        Team::create(['name' => 'Team D']);
        Team::create(['name' => 'Team E']);
        Team::create(['name' => 'Team F']);
        Team::create(['name' => 'Team G']);
        Team::create(['name' => 'Team H']);
        Team::create(['name' => 'Team I']);
        Team::create(['name' => 'Team J']);
    }
}
