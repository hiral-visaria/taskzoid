<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $fillable = [
        'ticket_id',
        'member_id',
        'query'
    ];

    public function getCreatedDateAttribute() {
        return $this->created_at->diffForHumans();
    }

    public function user() {
        return $this->belongsTo(User::class, 'member_id');
    }
}
