<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserCreationNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->user->team_id != null)
        {
            return (new MailMessage)
                        ->line("Your Details")
                        ->line("Name : {$this->user->name}")
                        ->line("Email : {$this->user->email}")
                        ->line("Temporary Password : {$this->password}")
                        ->line("Team: {$this->user->team->name}")
                        ->line("Role: {$this->user->role}")
                        ->line('Thank you for using our application!');
        }
        else
        {
            return (new MailMessage)
                        ->line("Your Details")
                        ->line("Name : {$this->user->name}")
                        ->line("Email : {$this->user->email}")
                        ->line("Temporary Password : {$this->password}")
                        ->line("Role: {$this->user->role}")
                        ->line('Thank you for using our application!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
