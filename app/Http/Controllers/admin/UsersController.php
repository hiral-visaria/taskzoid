<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Notifications\UserAddedToTeamNotification;
use App\Notifications\UserCreationNotification;
use Illuminate\Support\Str;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $this->authorize('view', User::class);
        return view('admin.users.index');
    }

    public function getUsers(){
        $users = User::where('role', 'naive')->get();
        return \DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('actions', function(User $user){
                    $actionButtons = "";
                    if(auth()->user()->can('update', $user)){
                        $actionButtons .= "
                                <a class='btn btn-outline-info btn-sm m-1' href='" . route('cpanel.users.addToTeamView', $user->id) . "'><i class='fa fa-users'></i>
                                    Add to Team
                                </a>";
                    }
                    return $actionButtons;
                })
                ->rawColumns(['actions'])
                ->make(true);
    }

    public function create()
    {
        $this->authorize('create', User::class);
        $teams = Team::with(['users' => function($query){
            $query->where('role', 'leader');
        }])->get();

        return view('admin.users.create', compact([
            'teams'
        ]));
    }

    public function store(CreateUserRequest $request)
    {
        $this->authorize('create', User::class);

        $team = Team::where('id', $request->team_id)->get();
        if($request->role === "leader") {
            $user = User::where('team_id', $request->team_id)->where('role', 'leader');
            if($user !== null) {
                session()->flash('error', $request->name . ' cannot be assigned as team leader of ' . $team[0]->name . ' as it already has a leader!');
                return redirect(route('users.index'));
            }
        }

        $image = '';
        if ($request->hasFile('image')) {
            $image = $request->image->store('users');
        }

        if($request->role)
            $role = $request->role;
        else
            $role = 'naive';

        $password = Str::random(8);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $role,
            'image' => $image,
            'team_id' => $request->team_id,
            'password' => Hash::make($password)
        ]);

        $user->notify(new UserCreationNotification($user, $password));

        session()->flash('success', "User Created Successfully");
        return redirect(route('users.index'));
    }

    public function addToTeamView(User $user) {
        $this->authorize('addToTeam', $user);
        $teams = Team::with(['users' => function($query){
            $query->where('role', 'leader');
        }])->get();

        return view('admin.users.addToTeamView', compact([
            'user',
            'teams'
        ]));
    }

    public function addToTeam(Request $request, User $user) {
        $this->authorize('addToTeam', User::class);

        $team = Team::where('id', $request->team_id)->first();
        if($request->role == "leader") {
            $leader = User::where('team_id', $request->team_id)->where('role', 'leader')->first();
            if($leader) {
                session()->flash('error', $request->name . ' cannot be assigned as team leader of "' . $team->name . '" as it already has a leader!');
                return redirect(route('users.index'));
            }
        }

        $user->update([
            'role'=>$request->role,
            'team_id'=>$request->team_id
        ]);

        $user->notify(new UserAddedToTeamNotification($user));

        session()->flash('success', $user->name . ' is now a "' . $request->role . '" of team "' . $team->name . '" !');
        return redirect(route('users.index'));
    }
}
