<?php

namespace App\Http\Controllers\leader;

use App\Http\Controllers\Controller;
use App\Log;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index() {
        $user = User::where('id', auth()->user()->id)->first();
        $members = User::where('team_id', auth()->user()->team_id)->where('role', 'member')->get();
        $projects = Project::where('team_id', auth()->user()->team_id)->get();

        $membersID = $members->pluck('id')->toArray();

        if($membersID) {
            $performance = DB::select(DB::raw("SELECT sum(((approved_tickets - unresolved_tickets) / total_tickets) * 100) / " . sizeof($membersID) . " as ticketCalculation FROM `performances` p1 WHERE `member_id` in (" . implode(', ', $membersID). ") ORDER BY ticketCalculation DESC"));
        } else {
            $performance = array();
        }

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.dashboard', compact([
            'user',
            'members',
            'membersID',
            'projects',
            'performance',
            'notifications'
        ]));
    }

    public function getLogs() {
        return \DataTables::of(Log::where('member_id', auth()->user()->id)->get())
            ->addIndexColumn()
            ->make(true);
    }

    public function getMembers() {
        $users = User::where('team_id', auth()->user()->team_id)->where('role', 'member')->get();
        if($users) {
            return \DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('image', function (User $user) {
                    if ($user->image)
                        return '<img src=' . asset("storage/" . $user->image) . ' alt="Student Image" width="128">';
                    else
                        return '<img src=' . asset("storage/users/neutral.png") . ' alt="Student Image" width="128">';
                })
                ->addColumn('performance', function (User $user) {
                    $performance = DB::select(DB::raw("SELECT member_id, ((approved_tickets - unresolved_tickets) / total_tickets) * 100 as ticketCalculation FROM `performances` p1 WHERE currently_assigned_tickets in (SELECT MIN(`currently_assigned_tickets`) FROM `performances` WHERE `member_id` = $user->id) AND `member_id` = $user->id ORDER BY ticketCalculation DESC"));
                    return sizeof($performance) ? $user->percentage(round($performance[0]->ticketCalculation, 2)) : $user->percentage(0);
                })
                ->addColumn('actions', function (User $user) {
                    $actionButtons = "<div class='d-flex'>";

                    if (auth()->user()->can('view', User::class)) {
                        $actionButtons .= "<a href='" . route('leader.member.dashboard', $user->id) . "' class='btn btn-icon btn-outline-success mr-1'><i class='fa fa-eye'></i></a>";
                    }
                    return $actionButtons . '</div>';
                })
                ->rawColumns(['image', 'performance', 'actions'])
                ->make(true);
        }
    }
}
