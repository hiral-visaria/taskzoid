@extends('layouts.template')

@section('title', 'Member | Manage Tickets')

@section('content')
    @include('layouts.partials._message')

    <div class="row">
        <div class="col-xl-12">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link" id="unresolved-tab" data-toggle="tab" href="#unresolved" role="tab" aria-controls="unresolved" aria-selected="false">Unresolved</a>
                    <a class="nav-item nav-link" id="assigned-tab" data-toggle="tab" href="#assigned" role="tab" aria-controls="assigned" aria-selected="true">Assigned</a>
                    <a class="nav-item nav-link" id="resolved-tab" data-toggle="tab" href="#resolved" role="tab" aria-controls="resolved" aria-selected="false">Resolved</a>
                    <a class="nav-item nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">Approved</a>
                </div>
            </nav>

            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade" id="unresolved" role="tabpanel" aria-labelledby="unresolved-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($unresolvedTickets))
                            @foreach ($unresolvedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('member.tickets.show', [$project->id, $ticket->id]) }}">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Unresolved Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade" id="assigned" role="tabpanel" aria-labelledby="assigned-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($assignedTickets))
                            @foreach ($assignedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col-md-12 mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('member.tickets.show', [$project->id, $ticket->id]) }}">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>

                                                    <div>
                                                        <form action="{{ route('member.tickets.resolve', [$project->id, $ticket->id]) }}" method="POST" id="resolve" class="d-inline-block">
                                                            @csrf
                                                            @method('PUT')

                                                            <button type="submit" class="btn btn-outline-primary btn-icon mr-2"><i class="fas fa-check"></i></button>
                                                        </form>

                                                        <form action="{{ route('member.tickets.unresolve', [$project->id, $ticket->id]) }}" method="POST" id="unresolve" class="d-inline-block">
                                                            @csrf
                                                            @method('PUT')
                                                            <button type="submit" class="btn btn-outline-danger btn-icon"><i class="fas fa-times"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Asigned Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade" id="resolved" role="tabpanel" aria-labelledby="resolved-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($resolvedTickets))
                            @foreach ($resolvedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('member.tickets.show', [$project->id, $ticket->id]) }}">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                            <span class="btn btn-outline-success btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-clipboard-list"></i> {{ $ticket->resolved_date }}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Resolved Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab">
                    <div class="row mt-4 pl-2">
                        @if (sizeof($approvedTickets))
                            @foreach ($approvedTickets as $ticket)
                                <div class="col-md-12 mb-4">
                                    <div class="card shadow h-100 py-2" style="border-left: dashed 2px #CCC">
                                        <div class="card-body">
                                            <div class="row p-3">
                                                <div class="col mr-2 d-flex justify-content-between">
                                                    <div class="text-lg font-weight-bold text-gray-800 text-uppercase mb-1">
                                                        <a href="{{ route('member.tickets.show', [$project->id, $ticket->id]) }}">
                                                            {{ $ticket->title }}
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mr-2">
                                                    <div class="pb-3">
                                                        @php
                                                            $labels = explode(",", $ticket->labels);
                                                            $colors = ["success", "info", "warning", "danger", "primary"];
                                                        @endphp
                                                        @foreach ($labels as $label)
                                                            <span class="btn btn-sm btn-outline-{{ Arr::random($colors) }}">{{ $label }}</span>
                                                        @endforeach
                                                    </div>

                                                    <div class="pb-3 text-gray-600">
                                                        {{ $ticket->description }}
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-between pb-3">
                                                        <div>
                                                            <span class="btn btn-outline-danger btn-bold btn-sm btn-upper">
                                                                <i class="fa fa-clock"></i> {{ $ticket->due_date }}
                                                            </span>
                                                            <span class="btn btn-outline-warning btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-file-signature"></i> {{ $ticket->assigned_date }}
                                                            </span>
                                                            <span class="btn btn-outline-success btn-bold btn-sm btn-upper">
                                                                <i class="fas fa-clipboard-list"></i> {{ $ticket->resolved_date }}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center mt-5">
                                <p class="mb-0">No Approved Tickets</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('styles')
    <style>
        a:hover,
        a:visited,
        a:focus{
            text-decoration: none;
        }

        .d-inline-block {
            display: inline-block;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script>
        function displayModalForm($projectID, $ticketID) {
            var url = '/cpanel/project/' + $projectID + '/tickets/' + $ticketID;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection
