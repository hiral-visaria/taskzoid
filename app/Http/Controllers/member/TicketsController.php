<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use App\Notifications\TicketStatusNotification;
use App\Project;
use App\Query;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TicketsController extends Controller
{
    public function show(Project $project, Ticket $ticket) {
        $this->authorize('view', Ticket::class);

        $queries = Query::where('ticket_id', $ticket->id)->with('user')->get();

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('member.tickets.show', compact([
            'project',
            'ticket',
            'queries',
            'notifications'
        ]));
    }

    public function resolve(Project $project, Ticket $ticket)
    {
        $this->authorize('resolve', $ticket);

        $ticket->update([
            'status' => "resolved",
            'resolved_at' => now()
        ]);

        $member = User::where('id', auth()->user()->id)->first();
        if($member->performance->currently_assigned_tickets == 0) {
            session()->flash('error', "Updation of Ticket was Unsuccessfully");
            return redirect(route('member.projects.show', compact([
                'project'
            ])));
        } else {
            $member->performance->decrement('currently_assigned_tickets');
        }

        $leader = User::where('team_id', $project->team_id)->where('role', 'leader')->first();
        $leader->notify(new TicketStatusNotification($ticket, "resolved"));

        session()->flash('success', "Ticket Updated Successfully");
        return redirect(route('member.projects.show', compact([
            'project'
        ])));
    }

    public function unresolve(Project $project, Ticket $ticket)
    {
        $this->authorize('unresolve', $ticket);

        $ticket->update([
            'status' => "unresolved"
        ]);

        $member = User::where('id', auth()->user()->id)->first();
        if($member->performance->currently_assigned_tickets == 0) {
            session()->flash('error', "Updation of Ticket was Unsuccessfully");
            return redirect(route('member.projects.show', compact([
                'project'
            ])));
        } else {
            $member->performance->decrement('currently_assigned_tickets');
        }

        $member->performance->increment('unresolved_tickets');

        $leader = User::where('team_id', $project->team_id)->where('role', 'leader')->first();
        $leader->notify(new TicketStatusNotification($ticket, "unresolved"));

        session()->flash('success', "Ticket Updated Successfully");
        return redirect(route('member.projects.show', compact([
            'project'
        ])));
    }
}
