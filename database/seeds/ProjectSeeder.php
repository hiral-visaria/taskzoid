<?php

use App\Project;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = ['php', 'database', 'laravel', 'ui', 'testing', 'swing', 'bugs', 'core', 'web', 'c', 'c++', 'java', 'ruby'];
        for($i = 1; $i <= 25; $i++)
        {
            $project = new Project();
            $project->team_id = rand(1, 10);
            $project->name = Factory::create()->sentence(rand(2, 4));

            $project->description = Factory::create()->sentence(rand(7, 10));

            shuffle($tags);
            $projectTags = "";
            for($j = 0; $j <= rand(1, 3); $j++){
                $projectTags .= $tags[$j] . ", ";
            }
            $projectTags = substr($projectTags, 0, -2);
            $project->tags = $projectTags;

            $project->due = Carbon::now()->format('Y-m-d');

            $project->save();
        }
    }
}
