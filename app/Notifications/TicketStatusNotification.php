<?php

namespace App\Notifications;

use App\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TicketStatusNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, $status)
    {
        $this->ticket = $ticket;
        $this->status = $status;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->status == "resolved"){
            return (new MailMessage)
                        ->from("{$this->ticket->member->email}")
                        ->line("Ticket Resolved")
                        ->line("Title : {$this->ticket->title}")
                        ->line("Assigned To: {$this->ticket->member->name}")
                        ->line("Due: {$this->ticket->due}")
                        ->line("Assigned At: {$this->ticket->assigned_at}")
                        ->line("Resolved At: {$this->ticket->resolved_at}")
                        ->line("Thank you for using our application!");
        } else if ($this->status == "unresolved") {
            return (new MailMessage)
                    ->from("{$this->ticket->member->email}")
                    ->line("Ticket Unresolved")
                    ->line("Title : {$this->ticket->title}")
                    ->line("Assigned To: {$this->ticket->member->name}")
                    ->line("Due: {$this->ticket->due}")
                    ->line("Assigned At: {$this->ticket->assigned_at}")
                    ->line("Thank you for using our application!");
        } else if ($this->status == "approved") {
            return (new MailMessage)
                    ->line("Ticket Approved")
                    ->line("Title : {$this->ticket->title}")
                    ->line("Assigned To: {$this->ticket->member->name}")
                    ->line("Due: {$this->ticket->due}")
                    ->line("Assigned At: {$this->ticket->assigned_at}")
                    ->line("Resolved At: {$this->ticket->resolved_at}")
                    ->line("Thank you for using our application!");
        } else if ($this->status == "assigned") {
            return (new MailMessage)
                    ->line("New Ticket Assigned")
                    ->line("Title: {$this->ticket->title}")
                    ->line("Description: {$this->ticket->description}")
                    ->line("Labels: {$this->ticket->labels}")
                    ->line("Due: {$this->ticket->due}")
                    ->line('Thank you for using our application!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'ticket' => $this->ticket
        ];
    }
}
