<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Log;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    protected function redirectTo()
    {
        $role = Auth::user()->role;
        Log::create([
            'member_id' => Auth::user()->id,
            'login_time' => now(),
            'logged_at' => date("Y-m-d")
        ]);

        if($role != null && $role === "admin")
            return '/cpanel/dashboard';
        else if ($role != null && $role == "leader")
            return '/leader/dashboard';
        return '/members/dashboard';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout() {
        Auth::user()->logs()->update([
            'logout_time' => now()
        ]);

        Auth::logout();
        return redirect('/');
    }
}
