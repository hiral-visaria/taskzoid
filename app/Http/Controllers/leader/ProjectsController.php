<?php

namespace App\Http\Controllers\leader;

use App\Http\Controllers\Controller;
use App\Project;
use App\Team;
use App\Ticket;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index(Team $team = null)
    {
        $this->authorize('view', Project::class);

        if($team) {
            $projects = Project::where('team_id', $team->id)->oldest('due')->where('due', '>=', now())->get();
            $projects = $projects->merge(Project::where('team_id', $team->id)->latest('due')->where('due', '<', now())->get());
        } else {
            $projects = Project::where('team_id', auth()->user()->team_id)->oldest('due')->where('due', '>=', now())->get();
            $projects = $projects->merge(Project::where('team_id', auth()->user()->team_id)->latest('due')->where('due', '<', now())->get());
        }

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.projects.index', compact([
            'team',
            'projects',
            'notifications'
        ]));
    }

    public function show(Project $project) {
        $this->authorize('view', Ticket::class);
        $resolvedTickets = Ticket::where('project_id', $project->id)->where('status', 'resolved')->get();
        $unresolvedTickets = Ticket::where('project_id', $project->id)->where('status', 'unresolved')->get();
        $assignedTickets = Ticket::where('project_id', $project->id)->where('status', 'assigned')->get();
        $unassignedTickets = Ticket::where('project_id', $project->id)->where('status', 'unassigned')->get();
        $approvedTickets = Ticket::where('project_id', $project->id)->where('status', 'approved')->get();

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('leader.tickets.index', compact([
            'project',
            'resolvedTickets',
            'unresolvedTickets',
            'assignedTickets',
            'unassignedTickets',
            'approvedTickets',
            'notifications'
        ]));
    }
}
