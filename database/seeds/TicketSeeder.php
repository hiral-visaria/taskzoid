<?php

use App\Project;
use App\Ticket;
use App\User;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $labels = ['urgent', 'database', 'ui', 'testing', 'bugs', 'core', 'todo', 'web', 'student', 'admin'];
        $difficulty_level = ['low', 'moderate', 'high'];
        $status = ['unassigned', 'assigned', 'resolved', 'unresolved', 'approved'];

        for($i = 1; $i <= 100; $i++)
        {
            $ticket = new Ticket();
            $project_id = rand(1, 25);
            $ticket->project_id = $project_id;
            $ticket->title = Factory::create()->sentence(rand(3, 5));
            $ticket->description = Factory::create()->sentence(rand(25, 30));

            shuffle($labels);
            $ticketLabels = "";
            for($j = 0; $j <= rand(1, 3); $j++){
                $ticketLabels .= $labels[$j] . ", ";
            }
            $ticketLabels = substr($ticketLabels, 0, -2);
            $ticket->labels = $ticketLabels;

            $ticket->due = Carbon::now()->format('Y-m-d');

            shuffle($difficulty_level);
            $ticket->difficulty_level = $difficulty_level[0];

            shuffle($status);
            $ticket->status = $status[0];

            if($status[0] != 'unassigned')
            {
                $team_id = Project::where('id', $project_id)->pluck('team_id')->first();
                $member_id = User::where('team_id', $team_id)->where('role', '!=', 'leader')->pluck('id')->toArray();
                shuffle($member_id);
                $ticket->member_id = $member_id[0];
                $ticket->assigned_at = Carbon::now()->format('Y-m-d');
            }

            if($status[0] == 'resolved' || $status[0] == 'approved')
            {
                $ticket->resolved_at = Carbon::now()->format('Y-m-d');
            }

            $ticket->save();
        }
    }
}
