<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'member_id',
        'login_time',
        'logout_time',
        'logged_at'
    ];

    public function member() {
        return $this->belongsTo(User::class, 'member_id');
    }
}
