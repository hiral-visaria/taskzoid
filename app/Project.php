<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'team_id',
        'name',
        'description',
        'tags',
        'due',
        'attachments'
    ];

    protected $dates = [
        'due'
    ];


    public function getDueDateAttribute() {
        return $this->due->diffForHumans();
    }

    // public function DateIntensity() {
    //     if($this->due == new DateTime("tomorrow"))
    //         return "warning";

    //     if($this->start_date <= now())
    //         return "Started On : " . $this->starting_date;

    //     return $this->start_date->diffForHumans();
    // }

    public function team() {
        return $this->belongsTo(Team::class);
    }

    public function tickets() {
        return $this->hasMany(Ticket::class);
    }
}
