@extends('layouts.template')

@section('title', 'Admin | Manage Users')

@section('content')
    @include('layouts.partials._message')

    <div class="row">
        <div class="col-md-12 d-flex justify-content-end mb-3" id="create_user_button">
            <a href="{{ route('users.create') }}" class="btn btn-outline-primary">
                <i class="fa fa-plus"></i>
                Create User
            </a>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Manage Users</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="users_table" width="100%" cellspacing="0">
                    <div id="export-buttons"></div>
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <form action="#" method='POST' id='makeAdminForm'>
        @csrf
        @method('PUT')
    </form>
@endsection


@section('styles')
    <style>
        #create_user_button {
            padding-right: 1.8rem!important;
        }

        #export-buttons .dt-buttons.btn-group.flex-wrap {
            position: absolute;
            left: 84.4%;
            top: .4rem;
        }

        #export-buttons .btn.buttons-html5 {
            background-color: #FFF;
            border-color: #CCC;
            color: #333;
        }
    </style>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var TableDatatables = function(){
            var handleUsersTable = function(){
                var oTable = $('#users_table').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('cpanel.datatable.users') }}",
                    columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false },
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'role', name:'role'},
                        {data: 'actions', name:'actions', orderable: false, searchable: false}
                    ]
                });
                new $.fn.dataTable.Buttons(oTable, {
                    buttons:[
                        'copy', 'csv', 'pdf'
                    ]
                } );
                oTable.buttons().container()
                    .appendTo( $('#export-buttons') );
            }
            return {
                //main function to handle all the datatables
                init: function () {
                    handleUsersTable();
                }
            }
        }();
        jQuery(document).ready(function () {
            TableDatatables.init();
        });
    });
</script>
@endsection
