<?php

use App\Project;
use App\Query;
use App\Ticket;
use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class QuerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tickets = Ticket::where('status', '!=', 'unassigned')->get()->toArray();
        for($i = 1; $i <= 40; $i++)
        {
            shuffle($tickets);
            $query = new Query();
            $query->ticket_id = $tickets[0]['id'];
            $query->member_id = $tickets[0]['member_id'];
            $query->query = Factory::create()->sentence(rand(5, 25));
            $query->save();
        }

        for($i = 1; $i <= 30; $i++)
        {
            shuffle($tickets);
            $team_id = Project::where('id', $tickets[0]['project_id'])->pluck('team_id')->first();
            $leader_id = User::where('team_id', $team_id)->where('role', 'leader')->pluck('id')->first();
            $query = new Query();
            $query->ticket_id = $tickets[0]['id'];
            $query->member_id = $leader_id;
            $query->query = Factory::create()->sentence(rand(5, 25));
            $query->save();
        }
    }
}
