<?php

use App\Log;
use Illuminate\Database\Seeder;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 150; $i++)
        {
            $log = new Log();
            $log->member_id = rand(12, 43);

            $day = rand(0, 60);
            $minutes = rand(1, 30);
            $seconds = rand(1, 30);

            $log->login_time = \Carbon\Carbon::now()->addDays($day)->addMinutes($minutes)->addSeconds($seconds);

            $log->logout_time = \Carbon\Carbon::now()->addDays($day)->addMinutes(rand($minutes+1, 59))->addSeconds(rand($seconds+10, 59));

            $log->logged_at =  \Carbon\Carbon::now()->addDays($day)->format('Y-m-d');

            $log->save();
        }
    }
}
