<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
    public function edit(User $user) {
        $this->authorize('update', User::class);

        return view('admin.edit', compact([
            'user'
        ]));
    }

    public function update(UpdateUserRequest $request, User $user) {
        $this->authorize('update', User::class);

        $data = $request->only(['name', 'email']);

        if ($request->hasFile('image')) {
            $image = $request->image->store('users');
            $user->deleteImage();
            $data['image'] = $image;
        }

        $user->update($data);

        session()->flash('success', "Personal Details Updated Successfully");
        return redirect(route('cpanel.dashboard'));
    }
}
