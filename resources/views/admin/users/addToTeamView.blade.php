@extends('layouts.template')

@section('title', 'Admin | Add To Team')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- CARD HEADER -->
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-plus"></i> Add To Team
                    </h6>
                </div>
                <!-- END OF CARD HEADER -->

                <!-- CARD BODY -->
                <div class="card-body">
                    <form class="kt-form" action="{{ route('cpanel.users.addToTeam', $user->id)}}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="name">Name</label>
                                <input type="text" value="{{ old('name', $user->name) }}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" readonly>
                                @error('name')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label for="team_id">Team</label>
                                <div>
                                    <select class="form-control select2" id="team_id" name="team_id">
                                        <option selected disabled>Select Team</option>
                                        @foreach($teams as $team)
                                            <option value="{{$team->id}}" {{ ($team->id == old('team_id')) ? 'selected' : '' }}>{{$team->name}} - {{ sizeof($team->users) ? $team->users[0]->name : 'No Leader Assigned Yet'}}</option>
                                        @endforeach
                                    </select>
                                    @error('team_id')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="role">Role</label>
                                <div>
                                    <select class="form-control select2" id="role" name="role">
                                        <option selected disabled>Select Role</option>
                                        <option value="member">Member</option>
                                        <option value="leader">Leader</option>
                                    </select>
                                    @error('role')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-outline-primary btn-sm m-1"><i class='fa fa-plus'></i> Add</button>
                    </form>
                </div>
                <!-- END OF CARD BODY -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.select2').select2();
    </script>
@endsection
