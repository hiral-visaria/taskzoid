<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        @if (auth()->user()->isLeader() || auth()->user()->isMember())
            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bell fa-fw"></i>
                </a>
                <!-- Dropdown - Messages -->
                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">
                        Notifications
                    </h6>

                    <ul class="nav mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">All</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-tickets-tab" data-toggle="pill" href="#pills-tickets" role="tab" aria-controls="pills-tickets" aria-selected="false">Tickets</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-queries-tab" data-toggle="pill" href="#pills-queries" role="tab" aria-controls="pills-queries" aria-selected="false">Queries</a>
                        </li>
                        @if (auth()->user()->isLeader())
                            <li class="nav-item">
                                <a class="nav-link" id="pills-projects-tab" data-toggle="pill" href="#pills-projects" role="tab" aria-controls="pills-projects" aria-selected="false">Projects</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="pills-teams-tab" data-toggle="pill" href="#pills-teams" role="tab" aria-controls="pills-teams" aria-selected="false">Teams</a>
                            </li>
                        @endif
                    </ul>

                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                            @foreach($notifications as $notification)
                                @if($notification->type === "App\Notifications\TicketStatusNotification")
                                    <a href="@if (auth()->user()->isMember()) {{ route('member.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @else {{ route('leader.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @endif" class="dropdown-item d-flex align-items-center">
                                        <div class="font-weight-bold">
                                            <div class="text-truncate">
                                                Ticket {{ ucfirst($notification->data['ticket']['status']) }}: {{ $notification->data['ticket']['title'] }}
                                            </div>
                                            <div class="small text-gray-500">
                                                Due: {{ date('Y-m-d', strtotime($notification->data['ticket']['due'])) }}
                                            </div>
                                        </div>
                                    </a>
                                @endif

                                @if($notification->type === "App\Notifications\QueryNotification")
                                    <a href="@if (auth()->user()->isMember()) {{ route('member.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @else {{ route('leader.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @endif" class="dropdown-item d-flex align-items-center">
                                        <div class="font-weight-bold">
                                            <div class="text-truncate">
                                                Comment: {{ $notification->data['query']['query'] }}
                                            </div>
                                            <div class="small text-gray-500">
                                                On Ticket: {{ $notification->data['ticket']['title'] }}
                                            </div>
                                        </div>
                                    </a>
                                @endif

                                @if($notification->type === "App\Notifications\ProjectCreationNotification")
                                    <a href="{{ route('leader.projects.show', $notification->data['project']['id']) }}"class="dropdown-item d-flex align-items-center">
                                        <div class="font-weight-bold">
                                            <div class="text-truncate">
                                                Project Name: {{ ucfirst($notification->data['project']['name']) }}
                                            </div>
                                            <div class="small text-gray-500">
                                                Due: {{ date('Y-m-d', strtotime($notification->data['project']['due'])) }}
                                            </div>
                                        </div>
                                    </a>
                                @endif

                                @if($notification->type === "App\Notifications\UserAddedToTeamNotification" && auth()->user()->id != $notification->data['user']['id'])
                                    <a href="{{ route('leader.member.dashboard', $notification->data['user']['id']) }}" class="dropdown-item d-flex align-items-center">
                                        <div class="font-weight-bold">
                                            <div class="text-truncate">
                                                Member Name: {{ ucfirst($notification->data['user']['name']) }}
                                            </div>
                                            <div class="small text-gray-500">
                                                Email: {{ $notification->data['user']['email'] }}
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>

                        <div class="tab-pane fade" id="pills-tickets" role="tabpanel" aria-labelledby="pills-tickets-tab">
                            @if (sizeof($notifications))
                                @foreach($notifications as $notification)
                                    @if($notification->type === "App\Notifications\TicketStatusNotification")
                                        <a href="@if (auth()->user()->isMember()) {{ route('member.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @else {{ route('leader.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @endif" class="dropdown-item d-flex align-items-center">
                                            <div class="font-weight-bold">
                                                <div class="text-truncate">
                                                    Ticket {{ ucfirst($notification->data['ticket']['status']) }}: {{ $notification->data['ticket']['title'] }}
                                                </div>
                                                <div class="small text-gray-500">
                                                    Due: {{ date('Y-m-d', strtotime($notification->data['ticket']['due'])) }}
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @endif
                        </div>

                        <div class="tab-pane fade" id="pills-queries" role="tabpanel" aria-labelledby="pills-queries-tab">
                            @if (sizeof($notifications))
                                @foreach($notifications as $notification)
                                    @if($notification->type === "App\Notifications\QueryNotification")
                                        <a href="@if (auth()->user()->isMember()) {{ route('member.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @else {{ route('leader.tickets.show', [$notification->data['ticket']['project_id'], $notification->data['ticket']['id']]) }} @endif" class="dropdown-item d-flex align-items-center">
                                            <div class="font-weight-bold">
                                                <div class="text-truncate">
                                                    Comment: {{ $notification->data['query']['query'] }}
                                                </div>
                                                <div class="small text-gray-500">
                                                    On Ticket: {{ $notification->data['ticket']['title'] }}
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @endif
                        </div>

                        <div class="tab-pane fade" id="pills-projects" role="tabpanel" aria-labelledby="pills-projects-tab">
                            @if (sizeof($notifications))
                                @foreach($notifications as $notification)
                                    @if($notification->type === "App\Notifications\ProjectCreationNotification")
                                        <a href="{{ route('leader.projects.show', $notification->data['project']['id']) }}"class="dropdown-item d-flex align-items-center">
                                            <div class="font-weight-bold">
                                                <div class="text-truncate">
                                                    Project Name: {{ ucfirst($notification->data['project']['name']) }}
                                                </div>
                                                <div class="small text-gray-500">
                                                    Due: {{ date('Y-m-d', strtotime($notification->data['project']['due'])) }}
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @endif
                        </div>

                        <div class="tab-pane fade" id="pills-teams" role="tabpanel" aria-labelledby="pills-teams-tab">
                            @if (sizeof($notifications))
                                @foreach($notifications as $notification)
                                    @if($notification->type === "App\Notifications\UserAddedToTeamNotification" && auth()->user()->id != $notification->data['user']['id'])
                                        <a href="{{ route('leader.member.dashboard', $notification->data['user']['id']) }}" class="dropdown-item d-flex align-items-center">
                                            <div class="font-weight-bold">
                                                <div class="text-truncate">
                                                    Member Name: {{ ucfirst($notification->data['user']['name']) }}
                                                </div>
                                                <div class="small text-gray-500">
                                                    Email: {{ $notification->data['user']['email'] }}
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </li>
        @endif

        @if (!auth()->user()->isAdmin())
            <div class="topbar-divider d-none d-sm-block"></div>
        @endif

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if(auth()->user()->image == null)
                    <img class="img-profile rounded-circle" src="{{ asset("storage/users/neutral.png") }}" alt="image">
                @else
                    <img class="img-profile rounded-circle" src="{{ asset("storage/" . auth()->user()->image) }}" alt="image">
                @endif

                <span class="ml-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->name }}</span>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" target="_blank" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>
