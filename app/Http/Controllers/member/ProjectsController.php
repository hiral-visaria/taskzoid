<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use App\Project;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index()
    {
        $this->authorize('view', Project::class);
        $projects = Project::where('team_id', auth()->user()->team_id)->oldest('due')->where('due', '>=', now())->with(['tickets' => function($query) {
            $query->where('member_id', auth()->user()->id);
        }])->get();

        $projects = $projects->merge(Project::where('team_id', auth()->user()->team_id)->latest('due')->where('due', '<', now())->with(['tickets' => function($query) {
            $query->where('member_id', auth()->user()->id);
        }])->get());

        $projectsWithTicketsForMember = array();
        $i = 0;
        foreach($projects as $project)
        {
            if(sizeof($project->tickets))
            {
                $projectsWithTicketsForMember[$i++] = $project;
            }
        }

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('member.projects.index', compact([
            'projectsWithTicketsForMember',
            'notifications'
        ]));
    }

    public function show(Project $project) {
        $this->authorize('view', Ticket::class);
        $resolvedTickets = Ticket::where('project_id', $project->id)->where('status', 'resolved')->where('member_id', auth()->user()->id)->get();
        $unresolvedTickets = Ticket::where('project_id', $project->id)->where('status', 'unresolved')->where('member_id', auth()->user()->id)->get();
        $assignedTickets = Ticket::where('project_id', $project->id)->where('status', 'assigned')->where('member_id', auth()->user()->id)->get();
        $approvedTickets = Ticket::where('project_id', $project->id)->where('status', 'approved')->where('member_id', auth()->user()->id)->get();

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('member.tickets.index', compact([
            'project',
            'resolvedTickets',
            'unresolvedTickets',
            'assignedTickets',
            'approvedTickets',
            'notifications'
        ]));
    }
}
