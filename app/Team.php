<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Team extends Model
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name',
    ];

    /**
     * RELATIONSHIP FUNCIONS
     */
    public function users() {
        return $this->hasMany(User::class);
    }

    public function projects() {
        return $this->hasMany(Project::class);
    }

    /**
     * HELPER FUNCTION
     */
    public function performance($percentage) {
        if($percentage <= 30)
            return "<span class='text-danger font-weight-bold'>$percentage%</span>";
        else if ($percentage > 30 && $percentage < 75)
            return "<span class='text-warning font-weight-bold'>$percentage%</span>";

        return "<span class='text-success font-weight-bold'>$percentage%</span>";
    }
}
