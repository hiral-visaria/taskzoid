<?php

namespace App\Http\Controllers\leader;

use App\Http\Controllers\Controller;
use App\Http\Requests\Members\UpdateMemberRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Project;
use App\User;
use Illuminate\Http\Request;

class LeadersController extends Controller
{
    public function edit(User $user) {
        $this->authorize('update', User::class);

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);

        return view('leader.edit', compact([
            'user',
            'notifications'
        ]));
    }

    public function update(UpdateUserRequest $request, User $user) {
        $this->authorize('update', User::class);

        $data = $request->only(['name', 'email']);

        if ($request->hasFile('image')) {
            $image = $request->image->store('users');
            $user->deleteImage();
            $data['image'] = $image;
        }

        $user->update($data);

        session()->flash('success', "Personal Details Updated Successfully");
        return redirect(route('leader.dashboard'));
    }
}
