<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Himanshu Thakkar',
            'email' => 'himanshu@studylinkclasses.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'admin',
            'image'=> 'users/1.jpg'
        ]);

        User::create([
            'team_id' => 1,
            'name' => 'Jinal Thakkar',
            'email' => 'jinal@studylinkclasses.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'leader',
            'image'=> 'users/2.jpg'
        ]);

        User::create([
            'team_id' => 2,
            'name' => 'John Doe',
            'email' => 'john@gmail.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'leader',
            'image'=> 'users/3.jpg'
        ]);

        $faker = \Faker\Factory::create();

        for($i = 0, $j = 3, $k=4; $i < 8; $i++, $j++, $k++)
        {
            $user = new User();
            $user->team_id = $j;
            $user->name = $faker->name;
            $user->email = $faker->unique()->safeEmail;
            $user->password = Hash::make('abcd1234');
            $user->role = 'leader';
            $user->image = "users/$k.jpg";
            $user->save();
        }

        User::create([
            'team_id' => 1,
            'name' => 'Vidhi Parikh',
            'email' => 'vidhi@studylinkclasses.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $k++;
        User::create([
            'team_id' => 1,
            'name' => 'Yukta Peswani',
            'email' => 'yukta@studylinkclasses.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $k++;
        User::create([
            'team_id' => 1,
            'name' => 'Hiral Visaria',
            'email' => 'hiral@studylinkclasses.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $k++;
        User::create([
            'team_id' => 1,
            'name' => 'Shraddha Keniya',
            'email' => 'shraddha@studylinkclasses.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $k++;
        User::create([
            'team_id' => 2,
            'name' => 'Mansi Pandey',
            'email' => 'mansi@studylinkclasses.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $k++;
        User::create([
            'team_id' => 2,
            'name' => 'Jane Doe',
            'email' => 'janedoe@gmail.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $k++;
        User::create([
            'team_id' => 2,
            'name' => 'Jimmy Doe',
            'email' => 'jimmy.doe@gmail.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $k++;
        User::create([
            'team_id' => 2,
            'name' => 'Soham Moore',
            'email' => 'sohammore12@gmail.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'member',
            'image'=> "users/$k.jpg"
        ]);

        $j = $k;
        for($i = 3; $i <= 10; $i++)
        {
            for($k = 0; $k < 3; $k++, $j++)
            {
                $user = new User();
                $user->team_id = $i;
                $user->name = $faker->name;
                $user->email = $faker->unique()->safeEmail;
                $user->password = Hash::make('abcd1234');
                $user->role = 'member';
                $user->image = "users/$j.jpg";
                $user->save();
            }
        }

        for($i = 0; $i < 5; $i++, $j++)
        {
            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->unique()->safeEmail;
            $user->password = Hash::make('abcd1234');
            $user->image = "users/$j.jpg";
            $user->save();
        }
    }
}
