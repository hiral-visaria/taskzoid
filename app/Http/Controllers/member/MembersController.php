<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use App\Http\Requests\Members\UpdateMemberRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MembersController extends Controller
{
    public function edit(User $member) {
        $this->authorize('update', User::class);

        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->paginate(10);

        return view('member.edit', compact([
            'member',
            'notifications'
        ]));
    }

    public function update(UpdateUserRequest $request, User $member) {
        $this->authorize('update', User::class);

        $data = $request->only(['name', 'email']);

        if ($request->hasFile('image')) {
            $image = $request->image->store('users');
            $member->deleteImage();
            $data['image'] = $image;
        }

        $member->update($data);

        $projects = Project::whereIn('id', auth()->user()->tickets->pluck('project_id'))->get();

        $performance = DB::select(DB::raw("SELECT member_id, ((total_tickets - approved_tickets - unresolved_tickets) / total_tickets) * 100 as ticketCalculation FROM `performances` p1 WHERE currently_assigned_tickets in (SELECT MIN(`currently_assigned_tickets`) FROM `performances` WHERE `member_id` = 12) AND `member_id` = 12 ORDER BY ticketCalculation DESC"));

        session()->flash('success', "Personal Details Updated Successfully");
        return redirect(route('member.dashboard'));
    }
}
